import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Article} from "./components/article/Article";

export function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" component={Article} exact/>
            </Switch>
        </BrowserRouter>
    )
}

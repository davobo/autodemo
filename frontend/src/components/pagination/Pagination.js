import React, {useState} from 'react';

/**
 * Pagination element class that represents button group for page selection
 */
export function Pagination({pages, pageHandler}){
    const elements = [];
    for (let i = 1; i <= pages; i++) {
        elements.push(i);
    }
    return (
        <div className="text-center">
            <nav>
                <ul className='pagination'>{elements.map((page, index) => (
                    <Page key={index} index={index} page={page} pageHandler={pageHandler}/>
                ))}
                </ul>
            </nav>
        </div>
    );
}

function Page({page, index, pageHandler}){
    return(
        <li key={index} className='page-item'>
        <a onClick={() => pageHandler(page-1)}
           className='page-link'>
            {page}
        </a>
    </li>);
}

/**
 * Pagination element that represents button group used for defining page size
 */
export function PaginationSize({pageData, setPageData}){
    const [buttons, setButtons] = useState([5, 25, 50, 100]);

    return(
        <div className="col-md-6 pull-right">
            <div className="btn-group pull-right" role="group">
                {buttons.map((button, index) => (
                    <PaginationButtons key={index} index={index} button={button} setPaginationHandler={setPageData}/>
                ))}
            </div>
            <h4 className="pull-right">Items per page:&nbsp;&nbsp;</h4>
        </div>
    );
}

export function PaginationSizeHook({pageData, setPageData}){
    const [buttons, setButtons] = useState([5, 25, 50, 100]);

    return(
        <div className="col-md-6 pull-right">
            <div className="btn-group pull-right" role="group">
                {buttons.map((button, index) => (
                    <PaginationButtons key={index} index={index} button={button} setPaginationHandler={setPageData(...pageData, {size:button})}/>
                ))}
            </div>
            <h4 className="pull-right">Items per page:&nbsp;&nbsp;</h4>
        </div>
    );
}

function PaginationButtons({button, index, setPaginationHandler}){
    return(
        <button key={index} onClick={() => setPaginationHandler(button)} type="button"
                className="btn btn-default">{button}</button>
    );
}
import * as React from "react";
import {Pagination} from "../pagination/Pagination";

/**
 * Function used for data representation
 */
export function ArticleTable({articles, articlesHandler, pages, pageHandler}){

    async function deleteArticle(article) {
        console.log("Article Id is: "+article.articleId)

        await fetch('/api/sale/article/' + article.articleId, {method: 'DELETE'}).then(
            () => {
                if ((pages.numberOfElements-(pages.totalPages*pages.size))<2) {
                    pages.numberOfElements = pages.numberOfElements - 1;
                    pages.number = Math.ceil((pages.numberOfElements / pages.size)) - 1;
                }
                //articles.splice(articles.indexOf(article))
                articlesHandler();
            })
            .catch(err => console.error(err));
    }

    console.log("Articles: "+articles)

    return (
        <div className="container">
            <table className="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Serial</th>
                    <th>Price</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>{articles.map((article, index) => (
                    <ArticleEntity key={index} article={article} deleteArticle={deleteArticle}/>
                ))}</tbody>
            </table>
            <Pagination pages={pages.totalPages} pageHandler={pageHandler}/>
        </div>);
}

/**
 * Function that represents article entity
 */
function ArticleEntity({article, index, deleteArticle}){
    return (
        <tr>
            <td>{article.articleId}</td>
            <td>{article.partSerial.serialNum}</td>
            <td>{article.price}</td>
            <td>
                <button key={index} className="btn btn-danger" onClick={()=>deleteArticle(article)}>Delete</button>
            </td>
        </tr>
    );
}
import React, {useEffect, useState} from "react";
import {ArticleTable} from "./ArticleTable";
import {ArticleForm} from "./ArticleForm";

/**
 * Main class used for Article UI
 *
 * TODO: Add some structuring, decouple fetching and representation...
 */
export function Article(){
    const [articles, setArticles] = useState([]);
    const [pageData, setPageData] = useState({
            last: 1,
            totalPages: 1,
            size: 5,
            number: 0,
            numberOfElements: 1
    });

    async function fetchData(){
        console.log("Fetching data");
        try{
            const response = await  fetch('/api/sale/article?size=' + pageData.size +
                '&page=' + (pageData.number));
            const json = await response.json();
            setArticles(json._embedded.articleList);
            setPageData({
                last: json._links.last,
                totalPages: json.page.totalPages,
                size: json.page.size,
                number: json.page.number,
                numberOfElements: json.page.totalElements
            });
        }catch (error) {
            console.error(error);
        };
    }

    useEffect(() => {
        fetchData();
    }, [])

    function updatePageSize(pageSize){
        console.log("On update page size");
        pageData.size = pageSize;
        console.log(pageData);
        fetchData();
    }

    // Update page
    function updatePageNumber(pageNumber) {
        console.log("Update page number to: " + pageNumber);
        pageData.number = pageNumber;
        console.log(pageData);
        fetchData();
    }

    async function createArticle(article) {

        await fetch('/api/sale/article/partSerial/' + article.serial, {method: 'POST',
            headers: {
                'Content-Type': 'application/hal+json',
            },
            body: JSON.stringify({price: article.price})}).then(
            () => {
                fetchData();
            })
            .catch(err => console.error(err));
    }

    return (
        <div>
            <ArticleForm createArticle={createArticle} pageData={pageData} setPageData={updatePageSize}/>
            <ArticleTable articles={articles}  articlesHandler={fetchData}
                          pages={pageData} pageHandler={updatePageNumber}/>
        </div>
    );
}
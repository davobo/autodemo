import * as React from "react";
import {PaginationSize} from "../pagination/Pagination";
import {useReducer} from "react";

/**
 * Element that is used to enter new data. Pagination size buttons are placed in header
 */
export function ArticleForm({createArticle, pageData, setPageData}){
    const [article, setArticle] = useReducer(
        (serial, price) => ({...serial, ...price}),
        {
            serialNumber:'',
            price:''
        }
    );

    function handleChange(event) {
        console.log("Part serial: " + event.target.name + " Price: " + event.target.value)
        setArticle({[event.target.name]: event.target.value});
    }

    function handleSubmit(event){
        event.preventDefault();
        console.log("Serial: " + article.serialNumber);
        createArticle({serial: article.serialNumber, price: article.price});
    }

    return (
        <div className="container">
            <div className="panel panel-default">
                <div className="panel-heading">
                    <div className="row">
                        <h4 className="col-md-6">Create Article</h4>
                        <PaginationSize pageData={pageData} setPageData={setPageData}/>
                    </div>
                </div>
                <div className="panel-body">
                    <form className="form-inline">
                        <div className="col-md-2">
                            <input type="number" placeholder="Serial number" className="form-control" name="serialNumber"
                                   onChange={handleChange}/>
                        </div>
                        <div className="col-md-2">
                            <input type="number" placeholder="Price" className="form-control" name="price"
                                   onChange={handleChange}/>
                        </div>
                        <div className="col-md-2">
                            <button className="btn btn-success" onClick={handleSubmit}>Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}
package com.auto.demo.common;

public class Constants {
    public static final String INTEGRATION_TEST = "IntegrationTest";
    public static final String UNIT_TEST = "UnitTest";
}

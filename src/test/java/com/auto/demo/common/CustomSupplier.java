package com.auto.demo.common;

import com.auto.demo.database.model.sale.Article;
import com.auto.demo.database.model.sale.Discount;
import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.database.model.warehouse.Part;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CustomSupplier {
    public static Article createArticleWithId(Integer id) {
        return Article.builder()
                .articleId(id)
                .price(BigDecimal.valueOf(150))
                .build();
    }

    public static Part createPartWithId(Integer id) {
        return Part.builder()
                .partId(id)
                .name("engine2Test")
                .serialNum(1241)
                .dateManufactured(LocalDate.of(2015, 12, 20))
                .build();
    }

    public static Discount createDiscountWithId(Integer id) {
        return Discount.builder()
                .discountId(id)
                .start(LocalDate.of(2020, 01, 05))
                .end(LocalDate.of(2020, 01, 07))
                .percent(50f)
                .build();
    }

    public static Brand createBrandWithId(Integer id) {
        return Brand.builder()
                .brandId(id)
                .name("TestBrand")
                .build();
    }

    public static Car createCarWithId(Integer id) {
        return Car.builder()
                .carId(id)
                .name("testCar")
                .build();
    }
}

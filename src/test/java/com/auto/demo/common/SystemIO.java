package com.auto.demo.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Slf4j
public class SystemIO {
    public static String readFile(Resource resource) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            bufferedReader.lines().forEach(stringBuilder::append);
        } catch (IOException e) {
            log.error("Unable to read file {}.", resource.getFilename());
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}

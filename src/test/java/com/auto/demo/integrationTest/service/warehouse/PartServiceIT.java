package com.auto.demo.integrationTest.service.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.warehouse.PartService;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class PartServiceIT {
    @Resource
    private PartService partService;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created part ID
    private static int CREATED_PART_ID = 11;

    @Test
    public void getMultipleParts() {

        Page<Part> allParts = partService.findAll(PageRequest.of(0, 10));
        List<Part> listOfParts = allParts.getContent();
        assertEquals(7, allParts.getTotalElements(), "Database doesn't contain expected number of parts.");
        assertEquals(1, listOfParts.get(0).getPartId(), "Database doesn't contain part with id: " + listOfParts.get(0).getPartId());
        assertEquals(7, listOfParts.get(6).getPartId(), "Database doesn't contain part with id: " + listOfParts.get(6).getPartId());
        assertEquals(allParts.getTotalPages(), 1);
    }

    @Test
    public void getPart() {
        Part part = partService.findById(1);

        assertEquals(1, part.getPartId(), "Part with wrong Id:" + part.getPartId() + " returned from database");
        assertEquals("tyer", part.getName(), "Part with unexpected name.");
    }

    @Test
    @Order(1)
    public void addNewPart() {
        Part newPart = partService.create(CustomSupplier.createPartWithId(CREATED_PART_ID));
        CREATED_PART_ID = newPart.getPartId();
        assertNotNull(partService.findById(CREATED_PART_ID));
    }

    @Test
    @Order(2)
    public void updateExistingPart() {
        Part newPart = Part.builder().name("test").build();
        partService.update(CREATED_PART_ID, newPart);
        Part updatedPart = partService.findById(CREATED_PART_ID);

        assertEquals("test", updatedPart.getName());
    }

    @Test
    @Order(3)
    public void deletePart() {
        partService.delete(CREATED_PART_ID);
        assertThrows(ResourceNotFoundException.class, () -> partService.findById(CREATED_PART_ID));
    }
}

package com.auto.demo.integrationTest.service.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.warehouse.BrandService;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class BrandServiceIT {
    @Resource
    private BrandService brandService;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created brand ID
    private static int CREATED_BRAND_ID = 6;

    @Test
    public void getMultipleBrands() {

        Page<Brand> allBrands = brandService.findAll(PageRequest.of(0, 10));
        List<Brand> listOfBrands = allBrands.getContent();
        assertEquals(2, allBrands.getTotalElements(), "Database doesn't contain expected number of brands.");
        assertEquals(1, listOfBrands.get(0).getBrandId(), "Database doesn't contain brand with id: " + listOfBrands.get(0).getBrandId());
        assertEquals(2, listOfBrands.get(1).getBrandId(), "Database doesn't contain brand with id: " + listOfBrands.get(1).getBrandId());
        assertEquals(allBrands.getTotalPages(), 1);
    }

    @Test
    public void getBrand() {
        Brand brand = brandService.findById(1);

        assertEquals(1, brand.getBrandId(), "Brand with wrong Id:" + brand.getBrandId() + " returned from database");
        assertEquals("Audi", brand.getName(), "Brand with unexpected name.");
    }

    @Test
    @Order(1)
    public void addNewBrand() {
        Brand newBrand = brandService.create(CustomSupplier.createBrandWithId(CREATED_BRAND_ID));
        CREATED_BRAND_ID = newBrand.getBrandId();
        assertNotNull(brandService.findById(CREATED_BRAND_ID));
    }

    @Test
    @Order(2)
    public void updateExistingBrand() {
        Brand newBrand = Brand.builder().name("test").build();
        brandService.update(CREATED_BRAND_ID, newBrand);
        Brand updatedBrand = brandService.findById(CREATED_BRAND_ID);

        assertEquals("test", updatedBrand.getName());
    }

    @Test
    @Order(3)
    public void deleteBrand() {
        brandService.delete(CREATED_BRAND_ID);
        assertThrows(ResourceNotFoundException.class, () -> brandService.findById(CREATED_BRAND_ID));
    }
}

package com.auto.demo.integrationTest.service.sales;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.model.sale.Discount;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.sales.DiscountService;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class DiscountServiceIT {

    @Resource
    private DiscountService discountService;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created discount ID
    private static int CREATED_DISCOUNT_ID = 6;

    @Test
    public void getMultipleDiscounts() {

        Page<Discount> allDiscounts = discountService.findAll(PageRequest.of(0, 10));
        List<Discount> listOfDiscounts = allDiscounts.getContent();
        assertEquals(5, allDiscounts.getTotalElements(), "Database doesn't contain expected number of discounts.");
        assertEquals(1, listOfDiscounts.get(0).getDiscountId().longValue(), "Database doesn't contain discount with id: " + listOfDiscounts.get(0).getDiscountId());
        assertEquals(5, listOfDiscounts.get(4).getDiscountId().longValue(), "Database doesn't contain discount with id: " + listOfDiscounts.get(4).getDiscountId());
        assertEquals(allDiscounts.getTotalPages(), 1);
    }

    @Test
    public void getDiscount() {
        Discount discount = discountService.findById(1);

        assertEquals(1, discount.getDiscountId(), "Discount with wrong Id:" + discount.getDiscountId() + " returned from database");
        assertEquals(20, discount.getPercent(), "Discount with unexpected price.");
        assertEquals(1, discount.getArticle().getArticleId(), "Discount with unexpected article.");
    }

    @Test
    @Order(1)
    public void addNewDiscount() {
        int article = 6;
        Discount newDiscount = discountService.create(article, CustomSupplier.createDiscountWithId(CREATED_DISCOUNT_ID));
        CREATED_DISCOUNT_ID = newDiscount.getDiscountId();
        assertNotNull(discountService.findById(CREATED_DISCOUNT_ID));
    }

    @Test
    @Order(2)
    public void updateExistingDiscount() {
        Discount newDiscount = Discount.builder().percent(88f).build();
        discountService.update(CREATED_DISCOUNT_ID, newDiscount);
        Discount updatedDiscount = discountService.findById(CREATED_DISCOUNT_ID);

        assertEquals(88f, updatedDiscount.getPercent());
    }

    @Test
    @Order(3)
    public void deleteDiscount() {
        discountService.delete(CREATED_DISCOUNT_ID);
        assertThrows(ResourceNotFoundException.class, () -> discountService.findById(CREATED_DISCOUNT_ID));
    }
}

package com.auto.demo.integrationTest.service.sales;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.model.sale.Article;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.sales.ArticleService;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class ArticleServiceIT {

    @Resource
    private ArticleService articleService;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created article ID
    private static int CREATED_ARTICLE_ID = 7;

    @Test
    public void getMultipleArticles() {

        Page<Article> allArticles = articleService.findAll(PageRequest.of(0, 10));
        List<Article> listOfArticles = allArticles.getContent();
        assertEquals(6, allArticles.getTotalElements(), "Database doesn't contain expected number of articles.");
        assertEquals(1, listOfArticles.get(0).getArticleId().longValue(), "Database doesn't contain article with id: " + listOfArticles.get(0).getArticleId());
        assertEquals(6, listOfArticles.get(5).getArticleId().longValue(), "Database doesn't contain article with id: " + listOfArticles.get(5).getArticleId());
        assertEquals(allArticles.getTotalPages(), 1);
    }

    @Test
    public void getArticle() {
        Article article = articleService.findById(1);

        assertEquals(1L, article.getArticleId().longValue(), "Article with wrong Id:" + article.getArticleId() + " returned from database");
        assertEquals(BigDecimal.valueOf(100), article.getPrice(), "Article with unexpected price.");
        assertEquals(1234, article.getPartSerial().getSerialNum(), "Article with unexpected part serial.");
    }

    @Test
    @Order(1)
    public void addNewArticle() {
        int partSerial = 1240;
        Article newArticle = articleService.create(partSerial, CustomSupplier.createArticleWithId(CREATED_ARTICLE_ID));
        CREATED_ARTICLE_ID = newArticle.getArticleId();
        assertNotNull(articleService.findById(CREATED_ARTICLE_ID));
    }

    @Test
    @Order(2)
    public void updateExistingArticle() {
        Article newArticle = Article.builder().price(BigDecimal.valueOf(125)).build();
        articleService.update(CREATED_ARTICLE_ID, newArticle);
        Article updatedArticle = articleService.findById(CREATED_ARTICLE_ID);

        assertEquals(BigDecimal.valueOf(125), updatedArticle.getPrice());
    }

    @Test
    @Order(3)
    public void deleteArticle() {
        articleService.delete(CREATED_ARTICLE_ID);
        assertThrows(ResourceNotFoundException.class, () -> articleService.findById(CREATED_ARTICLE_ID));
    }
}

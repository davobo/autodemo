package com.auto.demo.integrationTest.service.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.warehouse.CarService;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class CarServiceIT {

    @Resource
    private CarService carService;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created car ID
    private static int CREATED_CAR_ID = 11;

    @Test
    public void getMultipleCars() {

        Page<Car> allCars = carService.findAll(PageRequest.of(0, 10));
        List<Car> listOfCars = allCars.getContent();
        assertEquals(10, allCars.getTotalElements(), "Database doesn't contain expected number of cars.");
        assertEquals(1, listOfCars.get(0).getCarId(), "Database doesn't contain car with id: " + listOfCars.get(0).getCarId());
        assertEquals(10, listOfCars.get(9).getCarId(), "Database doesn't contain car with id: " + listOfCars.get(9).getCarId());
        assertEquals(allCars.getTotalPages(), 1);
    }

    @Test
    public void getCar() {
        Car car = carService.findById(1);

        assertEquals(1, car.getCarId(), "Car with wrong Id:" + car.getCarId() + " returned from database");
        assertEquals("A3", car.getName(), "Car with unexpected name.");
    }

    @Test
    @Order(1)
    public void addNewCar() {
        int brandId = 1;
        int partId = 7;
        Car newCar = carService.create(brandId, partId, CustomSupplier.createCarWithId(CREATED_CAR_ID));
        CREATED_CAR_ID = newCar.getCarId();
        assertNotNull(carService.findById(CREATED_CAR_ID));
    }

    @Test
    @Order(2)
    public void updateExistingCar() {
        Car newCar = Car.builder().name("test").build();
        carService.update(CREATED_CAR_ID, newCar);
        Car updatedCar = carService.findById(CREATED_CAR_ID);

        assertEquals("test", updatedCar.getName());
    }

    @Test
    @Order(3)
    public void deleteCar() {
        carService.delete(CREATED_CAR_ID);
        assertThrows(ResourceNotFoundException.class, () -> carService.findById(CREATED_CAR_ID));
    }
}

package com.auto.demo.integrationTest.controller.sales;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.common.SystemIO;
import com.auto.demo.database.model.sale.Article;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.sales.ArticleService;
import com.auto.demo.service.warehouse.PartService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ArticleControllerIT {

    @Resource
    private ArticleService articleService;

    @Resource
    private PartService partService;

    @LocalServerPort
    int randomServerPort;

    private static RestTemplate restTemplate;

    @Resource
    ResourceLoader resourceLoader;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created article ID
    private static int CREATED_ARTICLE_ID = 3;

    @BeforeAll
    public static void setup() {
        restTemplate = new RestTemplateBuilder().basicAuthentication("sales", "prodaja").build();
    }

    @Test
    public void getSingleArticle() throws IOException {
        // Given
        org.springframework.core.io.Resource articleExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/sales/ArticleController/ResponseArticleExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(articleExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/sale/article/1", String.class);

        // Then
        assertNotNull(response, "Response returned for article with ID 1 is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    void getSingleArticleThatDoesNotExists() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource articleDoesNotExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/sales/ArticleController/ResponseArticleDoesNotExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(articleDoesNotExistsJsonResponse);
        final int articleId = 100;
        expectedJsonStringResponse = expectedJsonStringResponse.replace("{articleId}", String.valueOf(articleId));

        // When
        String response;
        try {
            response = restTemplate.getForObject("http://localhost:" + randomServerPort
                    + "/api/sale/article/" + articleId, String.class);
        } catch (
                HttpClientErrorException httpClientErrorException) {
            response = httpClientErrorException.getResponseBodyAsString();
        }

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response);
        responseToMatch.remove("timestamp");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(responseToMatch.toString()));
    }

    @Test
    void getMultipleArticles() throws Exception {
        // Given
        org.springframework.core.io.Resource articleExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/sales/ArticleController/ResponseMultipleArticlesExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(articleExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/sale/article", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    @Order(1)
    void createArticle() throws Exception {
        // Given
        Article article = CustomSupplier.createArticleWithId(7);
        int partSerial = 1240;

        // When
        String response = restTemplate.postForObject("http://localhost:" + randomServerPort
                + "/api/sale/article/partSerial/" + partSerial, article, String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseArticle = new JSONObject(response).getJSONObject("_embedded").getJSONObject("article");
        responseArticle.remove("_links");
        responseArticle.getJSONObject("partSerial").remove("_links");
        CREATED_ARTICLE_ID = responseArticle.getInt("articleId");
        ObjectMapper mapper = new ObjectMapper();
        String articleFromDatabase = mapper.writer().writeValueAsString(articleService.findById(CREATED_ARTICLE_ID));
        JSONObject expectedResponse = new JSONObject(articleFromDatabase);
        expectedResponse.getJSONObject("partSerial").remove("links");
        expectedResponse.remove("links");
        assertEquals(mapper.readTree(expectedResponse.toString()), mapper.readTree(responseArticle.toString()));
    }

    @Test
    @Order(2)
    void updateArticle() {
        // Given
        Article article = Article.builder().price(BigDecimal.valueOf(200)).build();
        int partSerial = 1240;
        article.setPartSerial(partService.findBySerialNumber(partSerial));

        // When
        restTemplate.put("http://localhost:" + randomServerPort
                + "/api/sale/article/" + CREATED_ARTICLE_ID, article);

        // Then
        Article updatedArticle = articleService.findById(CREATED_ARTICLE_ID);
        assertEquals(updatedArticle.getPrice(), BigDecimal.valueOf(200));
    }

    @Test
    @Order(3)
    void deleteArticle() throws Exception {
        // When
        restTemplate.delete("http://localhost:" + randomServerPort
                + "/api/sale/article/" + CREATED_ARTICLE_ID);

        // Then
        assertThrows(ResourceNotFoundException.class, () -> articleService.findById(CREATED_ARTICLE_ID));
    }
}

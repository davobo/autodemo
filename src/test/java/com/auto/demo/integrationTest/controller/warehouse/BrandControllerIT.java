package com.auto.demo.integrationTest.controller.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.common.SystemIO;
import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.warehouse.BrandService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BrandControllerIT {

    @Resource
    private BrandService brandService;

    @LocalServerPort
    int randomServerPort;

    private static RestTemplate restTemplate;

    @Resource
    ResourceLoader resourceLoader;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created brand ID
    private static int CREATED_BRAND_ID = 3;

    @BeforeAll
    public static void setup() {
        restTemplate = new RestTemplateBuilder().basicAuthentication("warehouse", "skladiste").build();
    }

    @Test
    public void getSingleBrand() throws IOException {
        // Given
        org.springframework.core.io.Resource brandExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/BrandController/ResponseBrandExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(brandExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/brand/1", String.class);

        // Then
        assertNotNull(response, "Response returned for brand with ID 1 is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    void getSingleBrandThatDoesNotExists() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource brandDoesNotExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/BrandController/ResponseBrandDoesNotExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(brandDoesNotExistsJsonResponse);
        final int brandId = 100;
        expectedJsonStringResponse = expectedJsonStringResponse.replace("{brandId}", String.valueOf(brandId));

        // When
        String response;
        try {
            response = restTemplate.getForObject("http://localhost:" + randomServerPort
                    + "/api/warehouse/brand/" + brandId, String.class);
        } catch (
                HttpClientErrorException httpClientErrorException) {
            response = httpClientErrorException.getResponseBodyAsString();
        }

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response);
        responseToMatch.remove("timestamp");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(responseToMatch.toString()));
    }

    @Test
    void getMultipleBrands() throws Exception {
        // Given
        org.springframework.core.io.Resource brandExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/BrandController/ResponseMultipleBrandsExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(brandExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/brand", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    @Order(1)
    void createBrand() throws Exception {
        // Given
        Brand brand = CustomSupplier.createBrandWithId(3);

        // When
        String response = restTemplate.postForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/brand", brand, String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseBrand = new JSONObject(response).getJSONObject("_embedded").getJSONObject("brand");
        responseBrand.remove("_links");
        CREATED_BRAND_ID = responseBrand.getInt("brandId");
        ObjectMapper mapper = new ObjectMapper();
        String brandFromDatabase = mapper.writer().writeValueAsString(brandService.findById(CREATED_BRAND_ID));
        JSONObject expectedResponse = new JSONObject(brandFromDatabase);
        expectedResponse.remove("links");
        assertEquals(mapper.readTree(expectedResponse.toString()), mapper.readTree(responseBrand.toString()));
    }

    @Test
    @Order(2)
    void updateBrand() {
        // Given
        Brand brand = Brand.builder().name("updatedBrand").build();

        // When
        restTemplate.put("http://localhost:" + randomServerPort
                + "/api/warehouse/brand/" + CREATED_BRAND_ID, brand);

        // Then
        Brand updatedBrand = brandService.findById(CREATED_BRAND_ID);
        assertEquals(updatedBrand.getName(), "updatedBrand");
    }

    @Test
    @Order(3)
    void deleteBrand() throws Exception {
        // When
        restTemplate.delete("http://localhost:" + randomServerPort
                + "/api/warehouse/brand/" + CREATED_BRAND_ID);

        // Then
        assertThrows(ResourceNotFoundException.class, () -> brandService.findById(CREATED_BRAND_ID));
    }
}

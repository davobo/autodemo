package com.auto.demo.integrationTest.controller.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.common.SystemIO;
import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.warehouse.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CarControllerIT {

    @Resource
    private CarService carService;

    @LocalServerPort
    int randomServerPort;

    private static RestTemplate restTemplate;

    @Resource
    ResourceLoader resourceLoader;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created car ID
    private static int CREATED_CAR_ID = 3;

    @BeforeAll
    public static void setup() {
        restTemplate = new RestTemplateBuilder().basicAuthentication("warehouse", "skladiste").build();
    }

    @Test
    public void getSingleCar() throws IOException {
        // Given
        org.springframework.core.io.Resource carExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/CarController/ResponseCarExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(carExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/car/1", String.class);

        // Then
        assertNotNull(response, "Response returned for car with ID 1 is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    void getSingleCarThatDoesNotExists() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource carDoesNotExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/CarController/ResponseCarDoesNotExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(carDoesNotExistsJsonResponse);
        final int carId = 100;
        expectedJsonStringResponse = expectedJsonStringResponse.replace("{carId}", String.valueOf(carId));

        // When
        String response;
        try {
            response = restTemplate.getForObject("http://localhost:" + randomServerPort
                    + "/api/warehouse/car/" + carId, String.class);
        } catch (
                HttpClientErrorException httpClientErrorException) {
            response = httpClientErrorException.getResponseBodyAsString();
        }

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response);
        responseToMatch.remove("timestamp");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(responseToMatch.toString()));
    }

    @Test
    void getMultipleCars() throws Exception {
        // Given
        org.springframework.core.io.Resource carExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/CarController/ResponseMultipleCarsExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(carExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/car", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    @Order(1)
    void createCar() throws Exception {
        // Given
        Car car = CustomSupplier.createCarWithId(11);
        int partId = 6;
        int brandId = 1;

        // When
        String response = restTemplate.postForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/car/brand/" + brandId + "/part/" + partId, car, String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseCar = new JSONObject(response).getJSONObject("_embedded").getJSONObject("car");
        responseCar.remove("_links");
        responseCar.getJSONObject("part").remove("_links");
        responseCar.getJSONObject("brand").remove("_links");
        CREATED_CAR_ID = responseCar.getInt("carId");
        ObjectMapper mapper = new ObjectMapper();
        String carFromDatabase = mapper.writer().writeValueAsString(carService.findById(CREATED_CAR_ID));
        JSONObject expectedResponse = new JSONObject(carFromDatabase);
        expectedResponse.remove("links");
        expectedResponse.getJSONObject("part").remove("links");
        expectedResponse.getJSONObject("brand").remove("links");
        assertEquals(mapper.readTree(expectedResponse.toString()), mapper.readTree(responseCar.toString()));
    }

    @Test
    @Order(2)
    void updateCar() {
        // Given
        Car car = Car.builder().name("updatedCar").build();

        // When
        restTemplate.put("http://localhost:" + randomServerPort
                + "/api/warehouse/car/" + CREATED_CAR_ID, car);

        // Then
        Car updatedCar = carService.findById(CREATED_CAR_ID);
        assertEquals(updatedCar.getName(), "updatedCar");
    }

    @Test
    @Order(3)
    void deleteCar() throws Exception {
        // When
        restTemplate.delete("http://localhost:" + randomServerPort
                + "/api/warehouse/car/" + CREATED_CAR_ID);

        // Then
        assertThrows(ResourceNotFoundException.class, () -> carService.findById(CREATED_CAR_ID));
    }
}

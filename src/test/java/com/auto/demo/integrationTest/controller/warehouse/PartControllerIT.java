package com.auto.demo.integrationTest.controller.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.common.SystemIO;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.warehouse.PartService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PartControllerIT {

    @Resource
    private PartService partService;

    @LocalServerPort
    int randomServerPort;

    private static RestTemplate restTemplate;

    @Resource
    ResourceLoader resourceLoader;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created part ID
    private static int CREATED_PAR_ID = 3;

    @BeforeAll
    public static void setup() {
        restTemplate = new RestTemplateBuilder().basicAuthentication("warehouse", "skladiste").build();
    }

    @Test
    public void getSinglePart() throws IOException {
        // Given
        org.springframework.core.io.Resource partExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/PartController/ResponsePartExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(partExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/part/1", String.class);

        // Then
        assertNotNull(response, "Response returned for part with ID 1 is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    void getSinglePartThatDoesNotExists() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource partDoesNotExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/PartController/ResponsePartDoesNotExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(partDoesNotExistsJsonResponse);
        final int partId = 100;
        expectedJsonStringResponse = expectedJsonStringResponse.replace("{partId}", String.valueOf(partId));

        // When
        String response;
        try {
            response = restTemplate.getForObject("http://localhost:" + randomServerPort
                    + "/api/warehouse/part/" + partId, String.class);
        } catch (
                HttpClientErrorException httpClientErrorException) {
            response = httpClientErrorException.getResponseBodyAsString();
        }

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response);
        responseToMatch.remove("timestamp");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(responseToMatch.toString()));
    }

    @Test
    void getMultipleParts() throws Exception {
        // Given
        org.springframework.core.io.Resource partExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/warehouse/PartController/ResponseMultiplePartsExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(partExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/part", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    @Order(1)
    void createPart() throws Exception {
        // Given
        Part part = CustomSupplier.createPartWithId(8);

        // When
        String response = restTemplate.postForObject("http://localhost:" + randomServerPort
                + "/api/warehouse/part", part, String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responsePart = new JSONObject(response).getJSONObject("_embedded").getJSONObject("part");
        responsePart.remove("_links");
        CREATED_PAR_ID = responsePart.getInt("partId");
        ObjectMapper mapper = new ObjectMapper();
        String partFromDatabase = mapper.writer().writeValueAsString(partService.findById(CREATED_PAR_ID));
        JSONObject expectedResponse = new JSONObject(partFromDatabase);
        expectedResponse.remove("links");
        assertEquals(mapper.readTree(expectedResponse.toString()), mapper.readTree(responsePart.toString()));
    }

    @Test
    @Order(2)
    void updatePart() {
        // Given
        Part part = Part.builder().name("engine2Update").build();

        // When
        restTemplate.put("http://localhost:" + randomServerPort
                + "/api/warehouse/part/" + CREATED_PAR_ID, part);

        // Then
        Part updatedPart = partService.findById(CREATED_PAR_ID);
        assertEquals(updatedPart.getName(), "engine2Update");
    }

    @Test
    @Order(3)
    void deletePart() throws Exception {
        // When
        restTemplate.delete("http://localhost:" + randomServerPort
                + "/api/warehouse/part/" + CREATED_PAR_ID);

        // Then
        assertThrows(ResourceNotFoundException.class, () -> partService.findById(CREATED_PAR_ID));
    }
}

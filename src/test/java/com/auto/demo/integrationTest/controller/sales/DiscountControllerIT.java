package com.auto.demo.integrationTest.controller.sales;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.common.SystemIO;
import com.auto.demo.database.model.sale.Discount;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.sales.ArticleService;
import com.auto.demo.service.sales.DiscountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@Tag(Constants.INTEGRATION_TEST)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DiscountControllerIT {
    @Resource
    private DiscountService discountService;

    @Resource
    private ArticleService articleService;

    @LocalServerPort
    int randomServerPort;

    private static RestTemplate restTemplate;

    @Resource
    ResourceLoader resourceLoader;

    // In order to reduce code in tests (create, update and delete) common variable is used to share created discount ID
    private static int CREATED_DISCOUNT_ID = 3;

    @BeforeAll
    public static void setup() {
        restTemplate = new RestTemplateBuilder().basicAuthentication("sales", "prodaja").build();
    }

    @Test
    public void getSingleDiscount() throws IOException {
        // Given
        org.springframework.core.io.Resource discountExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/sales/DiscountController/ResponseDiscountExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(discountExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/sale/discount/1", String.class);

        // Then
        assertNotNull(response, "Response returned for discount with ID 1 is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    void getSingleDiscountThatDoesNotExists() throws IOException, JSONException {
        // Given
        org.springframework.core.io.Resource discountDoesNotExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/sales/DiscountController/ResponseDiscountDoesNotExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(discountDoesNotExistsJsonResponse);
        final int discountId = 100;
        expectedJsonStringResponse = expectedJsonStringResponse.replace("{discountId}", String.valueOf(discountId));

        // When
        String response;
        try {
            response = restTemplate.getForObject("http://localhost:" + randomServerPort
                    + "/api/sale/discount/" + discountId, String.class);
        } catch (HttpClientErrorException httpClientErrorException) {
            response = httpClientErrorException.getResponseBodyAsString();
        }

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseToMatch = new JSONObject(response);
        responseToMatch.remove("timestamp");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(responseToMatch.toString()));
    }

    @Test
    void getMultipleDiscounts() throws Exception {
        // Given
        org.springframework.core.io.Resource discountExistsJsonResponse =
                resourceLoader.getResource("classpath:controller/sales/DiscountController/ResponseMultipleDiscountsExist.json");
        String expectedJsonStringResponse = SystemIO.readFile(discountExistsJsonResponse);

        // When
        String response = restTemplate.getForObject("http://localhost:" + randomServerPort
                + "/api/sale/discount", String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        // Remove random port
        response = response.replace(":" + randomServerPort, "");
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.readTree(expectedJsonStringResponse), mapper.readTree(response));
    }

    @Test
    @Order(1)
    void createDiscount() throws Exception {
        // Given
        Discount discount = CustomSupplier.createDiscountWithId(7);
        int articleId = 6;

        // When
        String response = restTemplate.postForObject("http://localhost:" + randomServerPort
                + "/api/sale/discount/article/" + articleId, discount, String.class);

        // Then
        assertNotNull(response, "Response returned from server is null.");
        JSONObject responseDiscount = new JSONObject(response).getJSONObject("_embedded").getJSONObject("discount");
        responseDiscount.remove("_links");
        responseDiscount.getJSONObject("article").remove("_links");
        responseDiscount.getJSONObject("article").getJSONObject("partSerial").remove("_links");
        CREATED_DISCOUNT_ID = responseDiscount.getInt("discountId");
        ObjectMapper mapper = new ObjectMapper();
        String discountFromDatabase = mapper.writer().writeValueAsString(discountService.findById(CREATED_DISCOUNT_ID));
        JSONObject expectedResponse = new JSONObject(discountFromDatabase);
        expectedResponse.getJSONObject("article").remove("links");
        expectedResponse.getJSONObject("article").getJSONObject("partSerial").remove("links");
        expectedResponse.remove("links");
        assertEquals(mapper.readTree(expectedResponse.toString()), mapper.readTree(responseDiscount.toString()));
    }

    @Test
    @Order(2)
    void updateDiscount() {
        // Given
        Discount discount = Discount.builder().percent(32f).build();
        int articleId = 6;
        discount.setArticle(articleService.findById(articleId));

        // When
        restTemplate.put("http://localhost:" + randomServerPort
                + "/api/sale/discount/" + CREATED_DISCOUNT_ID, discount);

        // Then
        Discount updatedDiscount = discountService.findById(CREATED_DISCOUNT_ID);
        assertEquals(updatedDiscount.getPercent(), 32);
    }

    @Test
    @Order(3)
    void deleteDiscount() {
        // When
        restTemplate.delete("http://localhost:" + randomServerPort
                + "/api/sale/discount/" + CREATED_DISCOUNT_ID);

        // Then
        assertThrows(ResourceNotFoundException.class, () -> discountService.findById(CREATED_DISCOUNT_ID));
    }
}

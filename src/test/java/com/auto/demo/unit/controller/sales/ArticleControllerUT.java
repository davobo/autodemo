package com.auto.demo.unit.controller.sales;

import com.auto.demo.common.Constants;
import com.auto.demo.controller.sales.ArticleController;
import com.auto.demo.database.model.sale.Article;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.exception.RestControllerAdvice;
import com.auto.demo.service.sales.ArticleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class ArticleControllerUT {

    private MockMvc mvc;

    private Part part;

    private Article firstArticle;

    @Mock
    private ArticleService articleService;

    @InjectMocks
    private ArticleController articleController;

    private JacksonTester<PagedModel<EntityModel<Article>>> jsonPage;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        mvc = MockMvcBuilders.standaloneSetup(articleController)
                .setControllerAdvice(new RestControllerAdvice())
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
        part = new Part(1, "tyer", 1234, LocalDate.of(2015, 12, 17));
        part.add(Link.of("http://localhost/api/warehouse/part/1"));
        firstArticle = new Article(1, part, BigDecimal.valueOf(100));
    }

    @AfterEach
    public void clean() {
        Mockito.reset(articleService);
    }

    @Test
    public void getAllArticles() throws Exception {
        // Given
        List<Article> articleList = new ArrayList<>();
        articleList.add(firstArticle);
        articleList.add(new Article(2, part, BigDecimal.valueOf(150)));
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Article> pageImpl = new PageImpl<>(articleList, pageable, 1);
        given(articleService.findAll(pageable)).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/sale/article");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Article> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/sale/article")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Article>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonPage.write(model).getJson());
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void getSingleArticle() throws Exception {
        // Given
        given(articleService.findById(1)).willReturn(firstArticle);

        // When
        ResultActions response = mvc.perform(get("/api/sale/article/1"));

        // Then
        validateResponse(response);
    }

    @Test
    public void getArticleWithWrongId() throws Exception {
        // Given
        given(articleService.findById(1)).willThrow(new ResourceNotFoundException("Article with id 1 not found"));

        // When
        ResultActions response = mvc.perform(get("/api/sale/article/1"));

        // Then
        response.andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())))

                .andExpect(jsonPath("$.message", is("Article with id 1 not found")));
    }

    @Test
    public void getArticleByPartSerial() throws Exception {
        // Given
        given(articleService.findByPartSerial(1234)).willReturn(firstArticle);

        // When
        ResultActions response = mvc.perform(get("/api/sale/article/partSerial/1234"));

        // Then
        validateResponse(response);
    }

    @Test
    public void getArticleOffer() throws Exception {
        // Given
        given(articleService.findByPartSerial(1234)).willReturn(firstArticle);

        // When
        ResultActions response = mvc.perform(get("/api/sale/article/partSerial/1234"));

        // Then
        validateResponse(response);
    }

    @Test
    public void createArticle() throws Exception {
        // Given
        Article article = new Article(null, null, BigDecimal.valueOf(100));
        given(articleService.create(any(), any())).willReturn(firstArticle);

        // When
        ResultActions response = mvc.perform(post("/api/sale/article/partSerial/1234").content(
                new ObjectMapper().writeValueAsString(article)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        validateResponse(response);
    }

    @Test
    public void updateArticleByPartSerial() throws Exception {
        // Given
        Article article = new Article(null, null, BigDecimal.valueOf(100));
        given(articleService.updateByPartSerial(any(), any())).willReturn(firstArticle);

        // When
        ResultActions response = mvc.perform(put("/api/sale/article/part/1").content(
                new ObjectMapper().writeValueAsString(article)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void updateArticle() throws Exception {
        // Given
        Article article = new Article(null, null, BigDecimal.valueOf(100));
        given(articleService.update(any(), any())).willReturn(firstArticle);

        // When
        ResultActions response = mvc.perform(put("/api/sale/article/1").content(
                new ObjectMapper().writeValueAsString(article)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void deleteArticle() throws Exception {
        // Given
        doNothing().when(articleService).delete(any());

        // When
        ResultActions response = mvc.perform(delete("/api/sale/article/1"));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    private void validateResponse(ResultActions response) throws Exception {
        response.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.articleId", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.price", Matchers.is(100)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.partSerial.name", Matchers.is("tyer")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.partSerial.partId", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.partSerial.serialNum", Matchers.is(1234)));
    }
}

package com.auto.demo.unit.controller.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.controller.warehouse.PartController;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.exception.RestControllerAdvice;
import com.auto.demo.service.warehouse.PartService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class PartControllerUT {
    private MockMvc mvc;

    private Part firstPart;

    @Mock
    private PartService partService;

    @InjectMocks
    private PartController partController;

    private JacksonTester<PagedModel<EntityModel<Part>>> jsonPartPage;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        mvc = MockMvcBuilders.standaloneSetup(partController)
                .setControllerAdvice(new RestControllerAdvice())
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
        firstPart = CustomSupplier.createPartWithId(1);
    }

    @AfterEach
    public void clean() {
        Mockito.reset(partService);
    }

    @Test
    public void getAllParts() throws Exception {
        // Given
        List<Part> partList = new ArrayList<>();
        partList.add(firstPart);
        Part secondPart = new Part(2, "Wheel", 1242, LocalDate.of(2015, 1, 20));
        partList.add(secondPart);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Part> pageImpl = new PageImpl<>(partList, pageable, 1);
        given(partService.findAll(pageable)).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/warehouse/part");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Part> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/warehouse/part")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Part>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonPartPage.write(model).getJson());
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void getSinglePart() throws Exception {
        // Given
        given(partService.findById(1)).willReturn(firstPart);

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/part/1"));

        // Then
        validateResponse(response);
    }

    @Test
    public void getPartWithWrongId() throws Exception {
        // Given
        given(partService.findById(1)).willThrow(new ResourceNotFoundException("Part with id 1 not found"));

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/part/1"));

        // Then
        response.andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())))

                .andExpect(jsonPath("$.message", is("Part with id 1 not found")));
    }

    @Test
    public void getPartBySerialNumber() throws Exception {
        // Given
        given(partService.findBySerialNumber(any())).willReturn(firstPart);

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/part/serial/1241"));

        // Then
        validateResponse(response);
    }

    @Test
    public void getPartByDate() throws Exception {
        // Given
        List<Part> partList = new ArrayList<>();
        partList.add(firstPart);
        Part secondPart = new Part(2, "Wheel", 1242, LocalDate.of(2015, 12, 20));
        partList.add(secondPart);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Part> pageImpl = new PageImpl<>(partList, pageable, 1);
        given(partService.findByDate(LocalDate.of(2015, 12, 20), pageable)).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/warehouse/part/date?date=2015-12-20");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Part> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/warehouse/part/date?date=2015-12-20")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Part>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonPartPage.write(model).getJson().replace("&page=0&size=20", ""));
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void createPart() throws Exception {
        // Given
        given(partService.create(any())).willReturn(firstPart);

        // When
        ResultActions response = mvc.perform(post("/api/warehouse/part").content(
                new ObjectMapper().writeValueAsString(firstPart)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        validateResponse(response);
    }

    @Test
    public void updatePart() throws Exception {
        // Given
        Part part = new Part(null, "503i", null, null);
        given(partService.update(any(), any())).willReturn(firstPart);

        // When
        ResultActions response = mvc.perform(put("/api/warehouse/part/1").content(
                new ObjectMapper().writeValueAsString(part)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void deletePart() throws Exception {
        // Given
        doNothing().when(partService).delete(any());

        // When
        ResultActions response = mvc.perform(delete("/api/warehouse/part/1"));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    private void validateResponse(ResultActions response) throws Exception {
        response.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.partId", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.name", Matchers.is("engine2Test")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.serialNum", Matchers.is(1241)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.dateManufactured", Matchers.is("2015-12-20")));
    }
}

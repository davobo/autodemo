package com.auto.demo.unit.controller.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.controller.warehouse.CarController;
import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.database.model.warehouse.CarAndBrand;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.exception.RestControllerAdvice;
import com.auto.demo.service.warehouse.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class CarControllerUT {

    private MockMvc mvc;

    private Car firstCar;

    @Mock
    private CarService carService;

    @InjectMocks
    private CarController carController;

    private JacksonTester<PagedModel<EntityModel<Car>>> jsonCarPage;

    private JacksonTester<PagedModel<EntityModel<CarAndBrand>>> jsonCarAndBrandPage;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        mvc = MockMvcBuilders.standaloneSetup(carController)
                .setControllerAdvice(new RestControllerAdvice())
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
        firstCar = CustomSupplier.createCarWithId(1);
        firstCar.setBrand(CustomSupplier.createBrandWithId(1));
        firstCar.setPart(CustomSupplier.createPartWithId(1));
    }

    @AfterEach
    public void clean() {
        Mockito.reset(carService);
    }

    @Test
    public void getAllCars() throws Exception {
        // Given
        List<Car> brandList = new ArrayList<>();
        brandList.add(firstCar);
        Car secondCar = new Car(2, "530i", null, null);
        secondCar.setBrand(new Brand(2, "BMW"));
        secondCar.setPart(new Part(2, "wingMirror", 1235, LocalDate.of(2015, 12, 17)));
        brandList.add(secondCar);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Car> pageImpl = new PageImpl<>(brandList, pageable, 1);
        given(carService.findAll(pageable)).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/warehouse/car");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Car> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/warehouse/car")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Car>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonCarPage.write(model).getJson());
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void getSingleCar() throws Exception {
        // Given
        given(carService.findById(1)).willReturn(firstCar);

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/car/1"));

        // Then
        validateResponse(response);
    }

    @Test
    public void getCarWithWrongId() throws Exception {
        // Given
        given(carService.findById(1)).willThrow(new ResourceNotFoundException("Car with id 1 not found"));

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/car/1"));

        // Then
        response.andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())))

                .andExpect(jsonPath("$.message", is("Car with id 1 not found")));
    }

    @Test
    public void getCarByName() throws Exception {
        // Given
        List<Car> carList = new ArrayList<>();
        carList.add(firstCar);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Car> pageImpl = new PageImpl<>(carList, pageable, 1);
        given(carService.findByName(any(), any())).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/warehouse/car/name/A3");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Car> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/warehouse/car/name/A3")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Car>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonCarPage.write(model).getJson());
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void createCar() throws Exception {
        // Given
        Car car = new Car(null, "503i", null, null);
        given(carService.create(any(), any(), any())).willReturn(firstCar);

        // When
        ResultActions response = mvc.perform(post("/api/warehouse/car/brand/1/part/1").content(
                new ObjectMapper().writeValueAsString(car)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        validateResponse(response);
    }

    @Test
    public void updateCar() throws Exception {
        // Given
        Car car = new Car(null, "503i", null, null);
        given(carService.update(any(), any())).willReturn(firstCar);

        // When
        ResultActions response = mvc.perform(put("/api/warehouse/car/1").content(
                new ObjectMapper().writeValueAsString(car)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void deleteCar() throws Exception {
        // Given
        doNothing().when(carService).delete(any());

        // When
        ResultActions response = mvc.perform(delete("/api/warehouse/car/1"));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void getAllParts() throws Exception {
        // Given
        List<CarAndBrand> brandList = new ArrayList<>();
        CarAndBrand firstPart = new CarAndBrand(firstCar.getName(), firstCar.getBrand().getName(), 1234L);
        CarAndBrand secondPart = new CarAndBrand("530i", "BMW", 1235L);
        brandList.add(firstPart);
        brandList.add(secondPart);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<CarAndBrand> pageImpl = new PageImpl<>(brandList, pageable, 1);
        given(carService.getParts(pageable)).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/warehouse/car/parts");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<CarAndBrand> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/warehouse/car/parts")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<CarAndBrand>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonCarAndBrandPage.write(model).getJson());
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void getPartsByCarAndBrand() throws Exception {
        // Given
        CarAndBrand carAndBrand = new CarAndBrand("Audi", "A3", 3L);
        given(carService.getPartsByCarAndBrand(any(), any())).willReturn(carAndBrand);

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/car/partsByCarAndBrand?car=Audi&brand=A3"));

        // Then
        response.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.brand_and_automobile", Matchers.is("A3 Audi")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.count", Matchers.is(3)));
    }

    private void validateResponse(ResultActions response) throws Exception {
        response.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.carId", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.name", Matchers.is("testCar")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.part.name", Matchers.is("engine2Test")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.part.serialNum", Matchers.is(1241)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.part.dateManufactured", Matchers.is("2015-12-20")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.brand.name", Matchers.is("TestBrand")));
    }
}

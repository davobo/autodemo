package com.auto.demo.unit.controller.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.controller.warehouse.BrandController;
import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.exception.RestControllerAdvice;
import com.auto.demo.service.warehouse.BrandService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class BrandControllerUT {

    private MockMvc mvc;

    private Brand firstBrand;

    @Mock
    private BrandService brandService;

    @InjectMocks
    private BrandController brandController;

    private JacksonTester<PagedModel<EntityModel<Brand>>> jsonPage;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        mvc = MockMvcBuilders.standaloneSetup(brandController)
                .setControllerAdvice(new RestControllerAdvice())
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
        firstBrand = new Brand(1, "Audi");
    }

    @AfterEach
    public void clean() {
        Mockito.reset(brandService);
    }

    @Test
    public void getAllBrands() throws Exception {
        // Given
        List<Brand> brandList = new ArrayList<>();
        brandList.add(firstBrand);
        Brand secondBrand = new Brand(2, "BMW");
        brandList.add(secondBrand);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Brand> pageImpl = new PageImpl<>(brandList, pageable, 1);
        given(brandService.findAll(pageable)).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/warehouse/brand");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Brand> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/warehouse/brand")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Brand>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonPage.write(model).getJson());
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void getSingleBrand() throws Exception {
        // Given
        given(brandService.findById(1)).willReturn(firstBrand);

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/brand/1"));

        // Then
        validateResponse(response);
    }

    @Test
    public void getBrandWithWrongId() throws Exception {
        // Given
        given(brandService.findById(1)).willThrow(new ResourceNotFoundException("Brand with id 1 not found"));

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/brand/1"));

        // Then
        response.andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())))

                .andExpect(jsonPath("$.message", is("Brand with id 1 not found")));
    }

    @Test
    public void getBrandByName() throws Exception {
        // Given
        given(brandService.findByName(any())).willReturn(firstBrand);

        // When
        ResultActions response = mvc.perform(get("/api/warehouse/brand/name/Audi"));

        // Then
        validateResponse(response);
    }

    @Test
    public void createBrand() throws Exception {
        // Given
        Brand brand = new Brand(null, "BMW");
        given(brandService.create(any())).willReturn(firstBrand);

        // When
        ResultActions response = mvc.perform(post("/api/warehouse/brand").content(
                new ObjectMapper().writeValueAsString(brand)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        validateResponse(response);
    }

    @Test
    public void updateBrand() throws Exception {
        // Given
        Brand brand = new Brand(null, "BMW");
        given(brandService.create(any())).willReturn(firstBrand);

        // When
        ResultActions response = mvc.perform(put("/api/warehouse/brand/1").content(
                new ObjectMapper().writeValueAsString(brand)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void deleteBrand() throws Exception {
        // Given
        doNothing().when(brandService).delete(any());

        // When
        ResultActions response = mvc.perform(delete("/api/warehouse/brand/1"));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    private void validateResponse(ResultActions response) throws Exception {
        response.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.brandId", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.name", Matchers.is("Audi")));
    }
}

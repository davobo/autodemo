package com.auto.demo.unit.controller.sales;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.controller.sales.DiscountController;
import com.auto.demo.database.model.sale.Article;
import com.auto.demo.database.model.sale.Discount;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.exception.RestControllerAdvice;
import com.auto.demo.service.sales.DiscountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class DiscountControllerUT {

    private MockMvc mvc;

    private Article article;

    private Discount firstDiscount;

    @Mock
    private DiscountService discountService;

    @InjectMocks
    private DiscountController discountController;

    private JacksonTester<PagedModel<EntityModel<Discount>>> jsonPage;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        mvc = MockMvcBuilders.standaloneSetup(discountController)
                .setControllerAdvice(new RestControllerAdvice())
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
        Part part = new Part(1, "tyer", 1234, LocalDate.of(2015, 12, 17));
        part.add(Link.of("http://localhost/api/warehouse/part/1"));
        article = new Article(1, part, BigDecimal.valueOf(100));
        article.add(Link.of("http://localhost/api/sales/article/1"));
        firstDiscount = new Discount(1, LocalDate.of(2020, 01, 05),
                LocalDate.of(2020, 01, 07), 20f, article);
    }

    @AfterEach
    public void clean() {
        Mockito.reset(discountService);
    }

    @Test
    public void getAllDiscounts() throws Exception {
        // Given
        List<Discount> discountList = new ArrayList<>();
        discountList.add(firstDiscount);
        Article secondArticle = CustomSupplier.createArticleWithId(2).add(Link.of("http://localhost/api/sales/article/2"));
        secondArticle.setPartSerial(CustomSupplier.createPartWithId(2).add(Link.of("http://localhost/api/warehouse/part/2")));
        Discount secondDiscount = CustomSupplier.createDiscountWithId(2);
        secondDiscount.setArticle(secondArticle);
        discountList.add(secondDiscount);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Discount> pageImpl = new PageImpl<>(discountList, pageable, 1);
        given(discountService.findAll(pageable)).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/sale/discount");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Discount> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/sale/discount")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Discount>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonPage.write(model).getJson());
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void getSingleDiscount() throws Exception {
        // Given
        given(discountService.findById(1)).willReturn(firstDiscount);

        // When
        ResultActions response = mvc.perform(get("/api/sale/discount/1"));

        // Then
        validateResponse(response);
    }

    @Test
    public void getDiscountWithWrongId() throws Exception {
        // Given
        given(discountService.findById(1)).willThrow(new ResourceNotFoundException("Discount with id 1 not found"));

        // When
        ResultActions response = mvc.perform(get("/api/sale/discount/1"));

        // Then
        response.andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())))

                .andExpect(jsonPath("$.message", is("Discount with id 1 not found")));
    }

    @Test
    public void getDiscountByStartDate() throws Exception {
        // Given
        LocalDate start = LocalDate.of(2020, 01, 05);
        List<Discount> discountList = new ArrayList<>();
        discountList.add(firstDiscount);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Discount> pageImpl = new PageImpl<>(discountList, pageable, 1);
        given(discountService.findByStart(any(), any())).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/sale/discount/start?date=2020-01-05");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Discount> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/sale/discount/start?date=2020-01-05")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Discount>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonPage.write(model).getJson().replace("&page=0&size=20", ""));
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void getDiscountByEndDate() throws Exception {
        // Given
        LocalDate start = LocalDate.of(2020, 01, 05);
        List<Discount> discountList = new ArrayList<>();
        discountList.add(firstDiscount);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Discount> pageImpl = new PageImpl<>(discountList, pageable, 1);
        given(discountService.findByEnd(any(), any())).willReturn(pageImpl);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("api/sale/discount/end?date=2020-01-05");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<Discount> assembler = new PagedResourcesAssembler<>(resolver, null);

        // When
        MockHttpServletResponse response = mvc.perform(get("/api/sale/discount/end?date=2020-01-05")).andReturn().getResponse();

        // Then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        PagedModel<EntityModel<Discount>> model = assembler.toModel(pageImpl);
        assertThat(response.getContentAsString()).isEqualTo(jsonPage.write(model).getJson().replace("&page=0&size=20", ""));
        RequestContextHolder.resetRequestAttributes();
    }

    @Test
    public void createDiscount() throws Exception {
        // Given
        Discount discount = new Discount(null, LocalDate.of(2020, 01, 05),
                LocalDate.of(2020, 01, 07), 40f, null);
        given(discountService.create(any(), any())).willReturn(firstDiscount);

        // When
        ResultActions response = mvc.perform(post("/api/sale/discount/article/2").content(
                new ObjectMapper().writeValueAsString(article)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        validateResponse(response);
    }

    @Test
    public void updateDiscountByArticleId() throws Exception {
        // Given
        Discount discount = new Discount(null, LocalDate.of(2020, 01, 05),
                LocalDate.of(2020, 01, 07), 40f, null);
        given(discountService.updateByArticleId(any(), any())).willReturn(firstDiscount);

        // When
        ResultActions response = mvc.perform(put("/api/sale/discount/article/2").content(
                new ObjectMapper().writeValueAsString(article)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void updateDiscount() throws Exception {
        // Given
        Discount discount = new Discount(null, LocalDate.of(2020, 01, 05),
                LocalDate.of(2020, 01, 07), 40f, null);
        given(discountService.update(any(), any())).willReturn(firstDiscount);

        // When
        ResultActions response = mvc.perform(put("/api/sale/discount/1").content(
                new ObjectMapper().writeValueAsString(discount)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void deleteDiscount() throws Exception {
        // Given
        doNothing().when(discountService).delete(any());

        // When
        ResultActions response = mvc.perform(delete("/api/sale/discount/1"));

        // Then
        response.andExpect(status().is2xxSuccessful());
    }

    private void validateResponse(ResultActions response) throws Exception {
        response.andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.discountId", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.percent", Matchers.is(20.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.article.price", Matchers.is(100)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.article.partSerial.name", Matchers.is("tyer")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.article.partSerial.partId", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].value.article.partSerial.serialNum", Matchers.is(1234)));
    }

}

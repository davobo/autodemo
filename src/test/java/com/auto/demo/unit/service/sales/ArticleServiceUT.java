package com.auto.demo.unit.service.sales;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.dao.sale.ArticleDAO;
import com.auto.demo.database.dao.sale.DiscountDAO;
import com.auto.demo.database.dao.warehouse.PartDAO;
import com.auto.demo.database.model.sale.Article;
import com.auto.demo.database.model.sale.Offer;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.sales.ArticleServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class ArticleServiceUT {

    private Article firstArticle;

    @Mock
    private PartDAO partDAO;

    @Mock
    private ArticleDAO articleDAO;

    @Mock
    private DiscountDAO discountDAO;

    @InjectMocks
    private ArticleServiceImpl articleServiceImpl;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        Part part = new Part(1, "tyer", 1234, LocalDate.of(2015, 12, 17));
        part.add(Link.of("http://localhost/api/warehouse/part/1"));
        firstArticle = new Article(1, part, BigDecimal.valueOf(100));
    }

    @AfterEach
    public void clean() {
        Mockito.reset(articleDAO);
        Mockito.reset(partDAO);
        Mockito.reset(discountDAO);
    }

    @Test
    public void findAllArticles() {
        // Given
        List<Article> articleList = new ArrayList<>();
        articleList.add(firstArticle);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Article> pageImpl = new PageImpl<>(articleList, pageable, 1);
        given(articleDAO.findAll(pageable)).willReturn(pageImpl);

        // When
        articleServiceImpl.findAll(pageable);

        // Then
        verify(articleDAO).findAll(pageable);
    }

    @Test
    public void findArticleById() {
        // Given
        given(articleDAO.findById(any())).willReturn(Optional.of(firstArticle));

        // When
        Article article = articleServiceImpl.findById(1);

        // Then
        assertThat(Objects.equals(article, firstArticle)).isTrue();
    }

    @Test
    public void findArticleByMissingId() {
        // Given
        given(articleDAO.findById(1)).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            Article article = articleServiceImpl.findById(1);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Article with id 1 not found");
    }

    @Test
    public void findArticleByPartSerial() {
        // Given
        given(articleDAO.findByPartSerial(any())).willReturn(Optional.of(firstArticle));

        // When
        Article article = articleServiceImpl.findByPartSerial(1234);

        // Then
        assertThat(Objects.equals(article, firstArticle)).isTrue();
    }

    @Test
    public void findArticleByMissingPartSerial() {
        // Given
        given(articleDAO.findByPartSerial(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            Article article = articleServiceImpl.findByPartSerial(1234);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Article with part serial number 1234 not found");
    }

    @Test
    public void getArticleOffer() {
        // Given
        List<ArticleDAO.offerInterface> offerList = new ArrayList<>();
        offerList.add(new OfferInterfeace());
        given(articleDAO.getOffer(any())).willReturn(offerList);

        // When
        Page<Offer> result = articleServiceImpl.getOffer(PageRequest.of(0, 20));

        // Then
        assertThat(result.getTotalElements()).isEqualTo(1);
        assertTrue(result.getContent().get(0).getDateManufactured().isEqual(LocalDate.of(2021, 8, 14)));
        assertThat(result.getContent().get(0).getPrice()).isEqualTo(new BigDecimal("10.00"));
    }

    @Test
    public void createArticle() {
        // Given
        Article newArticle = new Article();
        newArticle.setPrice(BigDecimal.valueOf(100));
        given(partDAO.findBySerialNum(any())).willReturn(Optional.of(CustomSupplier.createPartWithId(1)));
        given(articleDAO.save(any())).willReturn(firstArticle);

        // When
        Article result = articleServiceImpl.create(1241, newArticle);

        // Then
        assertThat(result.getArticleId()).isEqualTo(1);
        assertThat(result.getPartSerial().getSerialNum()).isEqualTo(1234);
        assertThat(result.getPrice()).isEqualTo(BigDecimal.valueOf(100));
    }

    @Test
    public void createArticleWithViolation() {
        // Given
        Article newArticle = new Article();
        given(partDAO.findBySerialNum(any())).willReturn(Optional.of(CustomSupplier.createPartWithId(1)));
        given(articleDAO.save(any())).willReturn(firstArticle);

        // When
        String response = "";
        try {
            articleServiceImpl.create(1234, newArticle);
        } catch (ConstraintViolationException cve) {
            response = cve.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "price: must not be null");
    }

    @Test
    public void createArticleWithMissingPartSerial() {
        // Given
        Article newArticle = new Article();
        newArticle.setPrice(BigDecimal.valueOf(100));
        given(partDAO.findBySerialNum(1241)).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            articleServiceImpl.create(1241, newArticle);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertThat(response).isEqualTo("Part with serial number 1241 not found");
    }

    @Test
    public void updateByPartSerial() {
        // Given
        Article secondArticle = new Article();
        Article thirdArticle = new Article();
        secondArticle.setPrice(BigDecimal.valueOf(100));
        BeanUtils.copyProperties(firstArticle, thirdArticle);
        thirdArticle.setPrice(BigDecimal.valueOf(100));
        given(articleDAO.findByPartSerial(any())).willReturn(Optional.of(firstArticle));
        given(articleDAO.save(any())).willReturn(thirdArticle);

        // When
        Article result = articleServiceImpl.updateByPartSerial(1241, secondArticle);

        // Then
        assertThat(Objects.equals(result, thirdArticle)).isTrue();
    }

    @Test
    public void updateByPartSerialWithMissingPart() {
        // Given
        Article secondArticle = new Article();
        secondArticle.setPrice(BigDecimal.valueOf(100));
        given(articleDAO.findByPartSerial(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            articleServiceImpl.updateByPartSerial(1241, secondArticle);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertThat(response).isEqualTo("Article with part 1241 not found");
    }

    @Test
    public void updateArticle() {
        // Given
        Article secondArticle = new Article();
        Article thirdArticle = new Article();
        secondArticle.setPrice(BigDecimal.valueOf(100));
        BeanUtils.copyProperties(firstArticle, thirdArticle);
        thirdArticle.setPrice(BigDecimal.valueOf(100));
        given(articleDAO.findById(any())).willReturn(Optional.of(firstArticle));
        given(articleDAO.save(any())).willReturn(thirdArticle);

        // When
        Article result = articleServiceImpl.update(1241, secondArticle);

        // Then
        assertThat(Objects.equals(result, thirdArticle)).isTrue();
    }

    @Test
    public void updateMissingArticle() {
        // Given
        Article secondArticle = new Article();
        secondArticle.setPrice(BigDecimal.valueOf(100));
        given(articleDAO.findByPartSerial(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            articleServiceImpl.update(1, secondArticle);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertThat(response).isEqualTo("Article with ID 1 not found");
    }

    @Test
    public void deleteArticle() {
        // Given
        doNothing().when(discountDAO).deleteByArticle(any());
        doNothing().when(articleDAO).deleteById(any());

        // When
        articleServiceImpl.delete(1);

        // Then
        verify(discountDAO).deleteByArticle(any());
        verify(articleDAO).deleteById(any());
    }

    @Test
    public void deleteArticleByPartId() {
        // Given
        given(articleDAO.findByPartId(any())).willReturn(Optional.of(firstArticle));
        doNothing().when(discountDAO).deleteByArticle(any());
        doNothing().when(articleDAO).deleteById(any());

        // When
        articleServiceImpl.deleteByPartId(1);

        // Then
        verify(discountDAO).deleteByArticle(any());
        verify(articleDAO).deleteById(any());
    }

    private static class OfferInterfeace implements ArticleDAO.offerInterface {

        @Override
        public Integer getPartSerial() {
            return 1;
        }

        @Override
        public Date getDateManufactured() {
            return new GregorianCalendar(2021, Calendar.AUGUST, 14).getTime();
        }

        @Override
        public BigDecimal getPrice() {
            return new BigDecimal("10.00");
        }
    }
}

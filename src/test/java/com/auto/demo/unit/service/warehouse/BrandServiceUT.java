package com.auto.demo.unit.service.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.dao.warehouse.BrandDAO;
import com.auto.demo.database.dao.warehouse.CarDAO;
import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.warehouse.BrandServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class BrandServiceUT {

    private Brand firstBrand;

    @Mock
    private BrandDAO brandDAO;

    @Mock
    private CarDAO carDAO;

    @InjectMocks
    private BrandServiceImpl brandServiceImpl;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        firstBrand = CustomSupplier.createBrandWithId(1);
    }

    @AfterEach
    public void clean() {
        Mockito.reset(brandDAO);
        Mockito.reset(carDAO);
    }

    @Test
    public void findAllBrands() {
        // Given
        List<Brand> brandList = new ArrayList<>();
        brandList.add(firstBrand);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Brand> pageImpl = new PageImpl<>(brandList, pageable, 1);
        given(brandDAO.findAll(pageable)).willReturn(pageImpl);

        // When
        brandServiceImpl.findAll(pageable);

        // Then
        verify(brandDAO).findAll(pageable);
    }

    @Test
    public void findBrandById() {
        // Given
        given(brandDAO.findById(any())).willReturn(Optional.of(firstBrand));

        // When
        Brand result = brandServiceImpl.findById(1);

        // Then
        assertThat(Objects.equals(result, firstBrand)).isTrue();
    }

    @Test
    public void findBrandByMissingId() {
        // Given
        given(brandDAO.findById(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            brandServiceImpl.findById(1);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Brand with id 1 not found");
    }

    @Test
    public void findBrandByName() {
        // Given
        given(brandDAO.findByNameIgnoreCase(any())).willReturn(Optional.of(firstBrand));

        // When
        Brand brand = brandServiceImpl.findByName("test");

        // Then
        assertThat(Objects.equals(brand, firstBrand)).isTrue();
    }

    @Test
    public void findBrandByNameWithMissingName() {
        // Given
        given(brandDAO.findById(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            Brand brand = brandServiceImpl.findByName("test");
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Brand with name test not found");
    }

    @Test
    public void createBrand() {
        // Given
        given(brandDAO.save(any())).willReturn(firstBrand);

        // When
        brandServiceImpl.create(firstBrand);

        // Then
        verify(brandDAO).save(any());
    }

    @Test
    public void updateBrand() {
        // Given
        Brand secondBrand = new Brand();
        secondBrand.setName("updatedBrand");
        Brand thirdBrand = new Brand();
        BeanUtils.copyProperties(firstBrand, thirdBrand);
        thirdBrand.setName("updatedBrand");
        given(brandDAO.findById(any())).willReturn(Optional.of(firstBrand));
        given(brandDAO.save(any())).willReturn(thirdBrand);

        // When
        Brand response = brandServiceImpl.update(1, secondBrand);

        // Then
        assertEquals(thirdBrand, response);
    }

    @Test
    public void updateBrandWithMissingId() {
        // Given
        Brand secondBrand = new Brand();
        secondBrand.setName("updatedBrand");
        Brand thirdBrand = new Brand();
        BeanUtils.copyProperties(firstBrand, thirdBrand);
        thirdBrand.setName("updatedBrand");
        given(brandDAO.findById(any())).willReturn(Optional.empty());
        given(brandDAO.save(any())).willReturn(thirdBrand);

        // When
        String response = "";
        try {
            brandServiceImpl.update(1, secondBrand);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Brand with id 1 not found");
    }

    @Test
    public void deleteBrand() {
        // Given
        doNothing().when(carDAO).deleteByBrand(any());
        doNothing().when(brandDAO).deleteById(any());

        // When
        brandServiceImpl.delete(1);

        // Then
        verify(carDAO).deleteByBrand(any());
        verify(brandDAO).deleteById(any());
    }
}

package com.auto.demo.unit.service.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.dao.warehouse.CarDAO;
import com.auto.demo.database.dao.warehouse.PartDAO;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.sales.ArticleService;
import com.auto.demo.service.warehouse.PartServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class PartServiceUT {
    private Part firstPart;

    @Mock
    private PartDAO partDAO;

    @Mock
    private CarDAO carDAO;

    @Mock
    private ArticleService articleService;

    @InjectMocks
    private PartServiceImpl partServiceImpl;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        firstPart = CustomSupplier.createPartWithId(1);
    }

    @AfterEach
    public void clean() {
        Mockito.reset(partDAO);
        Mockito.reset(carDAO);
        Mockito.reset(articleService);
    }

    @Test
    public void findAllParts() {
        // Given
        List<Part> partList = new ArrayList<>();
        partList.add(firstPart);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Part> pageImpl = new PageImpl<>(partList, pageable, 1);
        given(partDAO.findAll(pageable)).willReturn(pageImpl);

        // When
        partServiceImpl.findAll(pageable);

        // Then
        verify(partDAO).findAll(pageable);
    }

    @Test
    public void findPartById() {
        // Given
        given(partDAO.findById(any())).willReturn(Optional.of(firstPart));

        // When
        Part result = partServiceImpl.findById(1);

        // Then
        verify(partDAO).findById(1);
    }

    @Test
    public void findPartByMissingId() {
        // Given
        given(partDAO.findById(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            partServiceImpl.findById(1);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Part with id 1 not found");
    }

    @Test
    public void findPartByName() {
        // Given
        given(partDAO.findByNameIgnoreCase(any())).willReturn(Optional.of(firstPart));

        // When
        Part part = partServiceImpl.findByName("test");

        // Then
        verify(partDAO).findByNameIgnoreCase("test");
    }

    @Test
    public void findPartByNameWithMissingName() {
        // Given
        given(partDAO.findById(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            partServiceImpl.findByName("test");
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Part with name test not found");
    }

    @Test
    public void findPartBySerialNumber() {
        // Given
        given(partDAO.findBySerialNum(any())).willReturn(Optional.of(firstPart));

        // When
        partServiceImpl.findBySerialNumber(1234);

        // Then
        verify(partDAO).findBySerialNum(any());
    }

    @Test
    public void findPartBySerialNumberWithMissingPart() {
        // Given
        given(partDAO.findBySerialNum(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            partServiceImpl.findBySerialNumber(1234);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Part with serial number 1234 not found");
    }

    @Test
    public void findPartByDate() {
        // Given
        List<Part> partList = new ArrayList<>();
        partList.add(firstPart);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Part> pageImpl = new PageImpl<>(partList, pageable, 1);
        given(partDAO.findByDateManufactured(any(), any())).willReturn(Optional.of(pageImpl));

        // When
        partServiceImpl.findByDate(LocalDate.of(2015, 12, 20), pageable);

        // Then
        verify(partDAO).findByDateManufactured(any(), any());
    }

    @Test
    public void findPartByDateWithMissingPart() {
        // Given
        Pageable pageable = PageRequest.of(0, 20);
        given(partDAO.findByDateManufactured(any(), any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            partServiceImpl.findByDate(LocalDate.of(2015, 12, 20), pageable);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Part with manufacture date 2015-12-20 not found");
    }

    @Test
    public void createPart() {
        // Given
        given(partDAO.save(any())).willReturn(firstPart);

        // When
        partServiceImpl.create(firstPart);

        // Then
        verify(partDAO).save(any());
    }

    @Test
    public void updatePart() {
        // Given
        Part secondPart = new Part();
        secondPart.setName("updatedPart");
        Part thirdPart = new Part();
        BeanUtils.copyProperties(firstPart, thirdPart);
        thirdPart.setName("updatedPart");
        given(partDAO.findById(any())).willReturn(Optional.of(firstPart));
        given(partDAO.save(any())).willReturn(thirdPart);

        // When
        Part response = partServiceImpl.update(1, secondPart);

        // Then
        assertEquals(thirdPart, response);
    }

    @Test
    public void updateMissingPart() {
        // Given
        Part secondPart = new Part();
        secondPart.setName("updatedPart");
        Part thirdPart = new Part();
        BeanUtils.copyProperties(firstPart, thirdPart);
        thirdPart.setName("updatedPart");
        given(partDAO.findById(any())).willReturn(Optional.empty());
        given(partDAO.save(any())).willReturn(thirdPart);

        // When
        String response = "";
        try {
            partServiceImpl.update(1, secondPart);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Part with id 1 not found");
    }

    @Test
    public void deleteBrand() {
        // Given
        doNothing().when(articleService).deleteByPartId(any());
        doNothing().when(carDAO).deleteByPart(any());
        doNothing().when(partDAO).deleteById(any());

        // When
        partServiceImpl.delete(1);

        // Then
        verify(articleService).deleteByPartId(any());
        verify(carDAO).deleteByPart(any());
        verify(partDAO).deleteById(any());
    }
}

package com.auto.demo.unit.service.sales;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.dao.sale.ArticleDAO;
import com.auto.demo.database.dao.sale.DiscountDAO;
import com.auto.demo.database.model.sale.Article;
import com.auto.demo.database.model.sale.Discount;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.sales.DiscountServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class DiscountServiceUT {
    private Discount firstDiscount;

    @Mock
    private ArticleDAO articleDAO;

    @Mock
    private DiscountDAO discountDAO;

    @InjectMocks
    private DiscountServiceImpl discountServiceImpl;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        Part part = new Part(1, "tyer", 1234, LocalDate.of(2015, 12, 17));
        part.add(Link.of("http://localhost/api/warehouse/part/1"));
        Article article = new Article(1, part, BigDecimal.valueOf(100));
        firstDiscount = CustomSupplier.createDiscountWithId(1);
        firstDiscount.setArticle(article);
    }

    @AfterEach
    public void clean() {
        Mockito.reset(articleDAO);
        Mockito.reset(discountDAO);
    }

    @Test
    public void findAllDiscounts() {
        // Given
        List<Discount> discountList = new ArrayList<>();
        discountList.add(firstDiscount);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Discount> pageImpl = new PageImpl<>(discountList, pageable, 1);
        given(discountDAO.findAll(pageable)).willReturn(pageImpl);

        // When
        discountServiceImpl.findAll(pageable);

        // Then
        verify(discountDAO).findAll(pageable);
    }

    @Test
    public void findDiscountById() {
        // Given
        given(discountDAO.findById(any())).willReturn(Optional.of(firstDiscount));

        // When
        Discount discount = discountServiceImpl.findById(1);

        // Then
        assertThat(Objects.equals(discount, firstDiscount)).isTrue();
    }

    @Test
    public void findDiscountByMissingId() {
        // Given
        given(discountDAO.findById(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            Discount discount = discountServiceImpl.findById(1);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Discount with id 1 not found");
    }

    @Test
    public void findDiscountByStartDate() {
        // Given
        List<Discount> discountList = new ArrayList<>();
        discountList.add(firstDiscount);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Discount> pageImpl = new PageImpl<>(discountList, pageable, 1);
        given(discountDAO.findByStart(any(), any())).willReturn(pageImpl);

        // When
        discountServiceImpl.findByStart(PageRequest.of(0, 20), LocalDate.of(2020, 01, 05));

        // Then
        verify(discountDAO).findByStart(any(), any());
    }

    @Test
    public void findDiscountByEndDate() {
        // Given
        List<Discount> discountList = new ArrayList<>();
        discountList.add(firstDiscount);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Discount> pageImpl = new PageImpl<>(discountList, pageable, 1);
        given(discountDAO.findByStart(any(), any())).willReturn(pageImpl);

        // When
        discountServiceImpl.findByEnd(PageRequest.of(0, 20), LocalDate.of(2020, 01, 05));

        // Then
        verify(discountDAO).findByEnd(any(), any());
    }

    @Test
    public void findDiscountByArticleId() {
        // Given
        given(discountDAO.findByArticleId(any())).willReturn(Optional.of(firstDiscount));

        // When
        Discount discount = discountServiceImpl.findByArticleId(1234);

        // Then
        assertThat(Objects.equals(discount, firstDiscount)).isTrue();
    }

    @Test
    public void findDiscountByMissingArticleId() {
        // Given
        given(discountDAO.findByArticleId(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            Discount discount = discountServiceImpl.findByArticleId(1);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertThat(response).isEqualTo("Discount with article Id: 1 not found");
    }

    @Test
    public void createDiscount() {
        // Given
        Discount newDiscount = new Discount();
        newDiscount.setPercent(15.5f);
        newDiscount.setStart(LocalDate.of(2020, 01, 05));
        newDiscount.setEnd(LocalDate.of(2020, 01, 07));
        given(articleDAO.findById(any())).willReturn(Optional.of(CustomSupplier.createArticleWithId(1)));
        given(discountDAO.save(any())).willReturn(firstDiscount);


        // When
        Discount result = discountServiceImpl.create(1, newDiscount);

        // Then
        assertThat(Objects.equals(result, firstDiscount)).isTrue();
    }

    @Test
    public void createDiscountWithConstraintViolation() {
        // Given
        Discount newDiscount = new Discount();
        newDiscount.setPercent(15.5f);
        newDiscount.setStart(LocalDate.of(2020, 01, 05));
        given(articleDAO.findById(any())).willReturn(Optional.of(CustomSupplier.createArticleWithId(1)));
        given(discountDAO.save(any())).willReturn(firstDiscount);

        // When
        String response = "";
        try {
            discountServiceImpl.create(1, newDiscount);
        } catch (ConstraintViolationException cve) {
            response = cve.getMessage();
        }

        // Then
        assertThat(response).isEqualTo("end: Please provide end date");
    }

    @Test
    public void createDiscountWithMissingArticleId() {
        // Given
        Discount newDiscount = new Discount();
        newDiscount.setPercent(15.5f);
        newDiscount.setStart(LocalDate.of(2020, 01, 05));
        newDiscount.setEnd(LocalDate.of(2020, 01, 07));
        given(articleDAO.findById(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            discountServiceImpl.create(1, newDiscount);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertThat(response).isEqualTo("Article with Id 1 not found");
    }

    @Test
    public void updateDiscountByArticleId() {
        // Given
        Discount secondDiscount = new Discount();
        secondDiscount.setPercent(11.1f);
        Discount thirdDiscount = new Discount();
        BeanUtils.copyProperties(firstDiscount, thirdDiscount);
        thirdDiscount.setPercent(11.1f);
        given(discountDAO.findByArticleId(any())).willReturn(Optional.of(firstDiscount));
        given(discountDAO.save(any())).willReturn(thirdDiscount);

        // When
        Discount response = discountServiceImpl.updateByArticleId(1, secondDiscount);

        // Then
        assertEquals(thirdDiscount, response);
    }

    @Test
    public void updateDiscountByArticleIdWithMissingArticleId() {
        // Given
        Discount secondDiscount = new Discount();
        secondDiscount.setPercent(11.1f);
        given(discountDAO.findByArticleId(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            discountServiceImpl.updateByArticleId(1, secondDiscount);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertThat(response).isEqualTo("Discount for article 1 not found");
    }

    @Test
    public void updateDiscount() {
        // Given
        Discount secondDiscount = new Discount();
        secondDiscount.setPercent(11.1f);
        Discount thirdDiscount = new Discount();
        BeanUtils.copyProperties(firstDiscount, thirdDiscount);
        thirdDiscount.setPercent(11.1f);
        given(discountDAO.findById(any())).willReturn(Optional.of(firstDiscount));
        given(discountDAO.save(any())).willReturn(thirdDiscount);

        // When
        Discount response = discountServiceImpl.update(1, secondDiscount);

        // Then
        assertEquals(thirdDiscount, response);
    }

    @Test
    public void updateDiscountWithMissingArticleId() {
        // Given
        Discount secondDiscount = new Discount();
        secondDiscount.setPercent(11.1f);
        given(discountDAO.findById(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            discountServiceImpl.update(1, secondDiscount);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertThat(response).isEqualTo("Discount with ID: 1 not found");
    }

    @Test
    public void deleteDiscountByArticleId() {
        // Given
        doNothing().when(discountDAO).deleteByArticle(any());

        // When
        discountServiceImpl.deleteByArticle(1);

        // Then
        verify(discountDAO).deleteByArticle(any());
    }

    @Test
    public void deleteDiscount() {
        // Given
        doNothing().when(discountDAO).deleteById(any());

        // When
        discountServiceImpl.delete(1);

        // Then
        verify(discountDAO).deleteById(any());
    }
}

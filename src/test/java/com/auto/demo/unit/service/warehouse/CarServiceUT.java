package com.auto.demo.unit.service.warehouse;

import com.auto.demo.common.Constants;
import com.auto.demo.common.CustomSupplier;
import com.auto.demo.database.dao.warehouse.CarDAO;
import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.database.model.warehouse.CarAndBrand;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.warehouse.BrandService;
import com.auto.demo.service.warehouse.CarServiceImpl;
import com.auto.demo.service.warehouse.PartService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@Tag(Constants.UNIT_TEST)
@ExtendWith(MockitoExtension.class)
public class CarServiceUT {
    private Car firstCar;

    @Mock
    private BrandService brandService;

    @Mock
    private PartService partService;

    @Mock
    private CarDAO carDAO;

    @InjectMocks
    private CarServiceImpl carServiceImpl;

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        Part part = CustomSupplier.createPartWithId(1);
        Brand brand = CustomSupplier.createBrandWithId(1);
        firstCar = CustomSupplier.createCarWithId(1);
        firstCar.setPart(part);
        firstCar.setBrand(brand);
    }

    @AfterEach
    public void clean() {
        Mockito.reset(brandService);
        Mockito.reset(partService);
        Mockito.reset(carDAO);
    }

    @Test
    public void findAllCars() {
        // Given
        List<Car> carList = new ArrayList<>();
        carList.add(firstCar);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Car> pageImpl = new PageImpl<>(carList, pageable, 1);
        given(carDAO.findAll(pageable)).willReturn(pageImpl);

        // When
        carServiceImpl.findAll(pageable);

        // Then
        verify(carDAO).findAll(pageable);
    }

    @Test
    public void findCarById() {
        // Given
        given(carDAO.findById(any())).willReturn(Optional.of(firstCar));

        // When
        Car result = carServiceImpl.findById(1);

        // Then
        assertThat(Objects.equals(result, firstCar)).isTrue();
    }

    @Test
    public void findCarByMissingId() {
        // Given
        given(carDAO.findById(any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            carServiceImpl.findById(1);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Car with id 1 not found");
    }

    @Test
    public void findCarByName() {
        // Given
        List<Car> carList = new ArrayList<>();
        carList.add(firstCar);
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Car> pageImpl = new PageImpl<>(carList, pageable, 1);
        given(carDAO.findByNameIgnoreCase(any(), any())).willReturn(pageImpl);

        // When
        carServiceImpl.findByName(pageable, "test");

        // Then
        verify(carDAO).findByNameIgnoreCase(pageable, "test");
    }

    @Test
    public void createCar() {
        // Given
        given(carDAO.save(any())).willReturn(firstCar);
        given(brandService.findById(any())).willReturn(CustomSupplier.createBrandWithId(1));
        given(partService.findById(any())).willReturn(CustomSupplier.createPartWithId(1));

        // When
        carServiceImpl.create(1, 1, firstCar);

        // Then
        verify(carDAO).save(any());
    }

    @Test
    public void createCarWithViolation() {
        // Given
        Car secondCar = new Car();
        secondCar.setName("testCar");
        given(carDAO.save(any())).willReturn(firstCar);
        given(brandService.findById(any())).willReturn(null);
        given(partService.findById(any())).willReturn(CustomSupplier.createPartWithId(1));

        // When
        String response = "";
        try {
            carServiceImpl.create(1, 1, secondCar);
        } catch (ConstraintViolationException cve) {
            response = cve.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "brand: Please provide brand");
    }

    @Test
    public void updateCar() {
        // Given
        Car secondCar = new Car();
        secondCar.setName("testCar");
        Car thirdCar = new Car();
        BeanUtils.copyProperties(firstCar, thirdCar);
        thirdCar.setName("testCar");
        given(carDAO.findById(any())).willReturn(Optional.of(firstCar));
        given(carDAO.save(any())).willReturn(thirdCar);

        // When
        Car response = carServiceImpl.update(1, secondCar);

        // Then
        assertEquals(thirdCar, response);
    }

    @Test
    public void updateCarWithMissingId() {
        // Given
        Car secondCar = new Car();
        secondCar.setName("testCar");
        Car thirdCar = new Car();
        BeanUtils.copyProperties(firstCar, thirdCar);
        thirdCar.setName("testCar");
        given(carDAO.findById(any())).willReturn(Optional.empty());
        given(carDAO.save(any())).willReturn(thirdCar);

        // When
        String response = "";
        try {
            carServiceImpl.update(1, secondCar);
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertNotNull(response, "Response returned from service is null.");
        assertEquals(response, "Car with id 1 not found");
    }

    @Test
    public void deleteCar() {
        // Given
        doNothing().when(carDAO).deleteById(any());

        // When
        carServiceImpl.delete(1);

        // Then
        verify(carDAO).deleteById(any());
    }

    @Test
    public void deleteCarByPartId() {
        // Given
        doNothing().when(carDAO).deleteByPart(any());

        // When
        carServiceImpl.deleteByPart(1);

        // Then
        verify(carDAO).deleteByPart(any());
    }

    @Test
    public void deleteCarByBrandId() {
        // Given
        doNothing().when(carDAO).deleteByBrand(any());

        // When
        carServiceImpl.deleteByBrand(1);

        // Then
        verify(carDAO).deleteByBrand(any());
    }

    @Test
    public void getCarParts() {
        // Given
        List<CarAndBrand> carAndBrandList = new ArrayList<>();
        carAndBrandList.add(new CarAndBrand("testCar", "test", 10L));
        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<CarAndBrand> pageImpl = new PageImpl<>(carAndBrandList, pageable, 1);
        given(carDAO.getParts(pageable)).willReturn(pageImpl);

        // When
        Page<CarAndBrand> response = carServiceImpl.getParts(pageable);

        // Then
        verify(carDAO).getParts(pageable);
    }

    @Test
    public void getCarPartsByCarAndBrand() {
        // Given
        CarAndBrand carAndBrand = new CarAndBrand("testCar", "test", 10L);
        given(carDAO.getPartsForCarAndBrand(any(), any())).willReturn(Optional.of(carAndBrand));

        // When
        CarAndBrand response = carServiceImpl.getPartsByCarAndBrand("testCar", "test");

        // Then
        assertEquals(carAndBrand, response);
    }

    @Test
    public void getCarPartsByCarAndBrandWithMissingCar() {
        // Given
        given(carDAO.getPartsForCarAndBrand(any(), any())).willReturn(Optional.empty());

        // When
        String response = "";
        try {
            carServiceImpl.getPartsByCarAndBrand("testCar", "test");
        } catch (ResourceNotFoundException rnfe) {
            response = rnfe.getMessage();
        }

        // Then
        assertThat(response).isEqualTo("There are no parts for car testCar and brand test");
    }

}

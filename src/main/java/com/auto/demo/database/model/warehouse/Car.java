package com.auto.demo.database.model.warehouse;

import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Car entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
@Table(name = "car")
public class Car extends RepresentationModel<Car> implements Serializable {
    /**
     * Auto incremented id filed for unique identification in table.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column
    private Integer carId;

    /**
     * String field for storing car name
     */
    @Column
    @NotBlank
    private String name;

    /**
     * FK field used to uniquely connect car to corresponding part
     */
    @ManyToOne
    @JoinColumn(name = "part")
    @NotNull(message = "Please provide part")
    private Part part;

    /**
     * FK field used to uniquely connect car to corresponding brand
     */
    @ManyToOne
    @JoinColumn(name = "brand")
    @NotNull(message = "Please provide brand")
    private Brand brand;

    /**
     * To provide meaningful printout in log default "toString" method is overridden
     *
     * @return String
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

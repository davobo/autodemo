package com.auto.demo.database.model.warehouse;

import com.auto.demo.controller.warehouse.serializer.PartsSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;

/**
 * CarAndBrand entity
 */
@Getter
@Setter
@JsonSerialize(using = PartsSerializer.class)
public class CarAndBrand extends RepresentationModel<CarAndBrand> implements Serializable {
    /**
     * Auto incremented id filed for unique identification in table.
     */
    private String car;

    /**
     * String field for storing brand
     */
    private String brand;

    /**
     * Long field for storing parts number
     */
    private Long parts;

    /**
     * Object constructor
     *
     * @param car   String
     * @param brand String
     * @param parts Long
     */
    public CarAndBrand(String car, String brand, Long parts) {
        this.car = car;
        this.brand = brand;
        this.parts = parts;
    }

    /**
     * To provide meaningful printout in log default "toString" method is overridden
     *
     * @return String
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

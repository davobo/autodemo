package com.auto.demo.database.model.warehouse;

import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Brand entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
@Table(name = "brand")
public class Brand extends RepresentationModel<Brand> implements Serializable {
    /**
     * Auto incremented id filed for unique identification in table.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column
    private Integer brandId;

    /**
     * String field for storing brand name
     */
    @Column
    @NotBlank
    private String name;

    /**
     * To provide meaningful printout in log default "toString" method is overridden
     *
     * @return String
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

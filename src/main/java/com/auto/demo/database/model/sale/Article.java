package com.auto.demo.database.model.sale;

import com.auto.demo.database.model.warehouse.Part;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Article entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
@Table(name = "article")
public class Article extends RepresentationModel<Article> implements Serializable {
    /**
     * Auto incremented id filed for unique identification in table.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "articleId")
    private Integer articleId;

    /**
     * Long field for storing part serial number
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "partSerial", referencedColumnName = "serialNum")
    @NotNull
    private Part partSerial;

    /**
     * BigDecimal field for storing price
     */
    @Column(name = "price")
    @NotNull
    private BigDecimal price;

    /**
     * To provide meaningful printout in log default "toString" method is overridden
     *
     * @return String
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

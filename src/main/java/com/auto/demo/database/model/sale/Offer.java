package com.auto.demo.database.model.sale;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

/**
 * Offer entity
 */
@Getter
@Setter
public class Offer extends RepresentationModel<com.auto.demo.database.model.warehouse.CarAndBrand> implements Serializable {
    /**
     * Auto incremented id filed for unique identification in table.
     */
    private Integer partSerial;

    /**
     * String field for storing manufactured date
     */
    private LocalDate dateManufactured;

    /**
     * BigDecimal field for storing price
     */
    private BigDecimal price;

    /**
     * Object constructor
     *
     * @param partSerial       Integer
     * @param dateManufactured Date
     * @param price            BigDecimal
     */
    public Offer(Integer partSerial, LocalDate dateManufactured, BigDecimal price) {
        this.partSerial = partSerial;
        this.dateManufactured = dateManufactured;
        this.price = price;
    }

    /**
     * To provide meaningful printout in log default "toString" method is overridden
     *
     * @return String
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.SHORT_PREFIX_STYLE);
    }
}


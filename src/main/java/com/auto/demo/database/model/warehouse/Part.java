package com.auto.demo.database.model.warehouse;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Part entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
@Table(name = "part")
public class Part extends RepresentationModel<Part> implements Serializable {

    /**
     * Auto incremented id filed for unique identification in table.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "partId")
    private Integer partId;

    /**
     * String field for storing part name
     */
    @NotBlank
    @Column(name = "name")
    private String name;

    /**
     * Field for storing part serial number.
     */
    @NotNull
    @Column(name = "SerialNum")
    private Integer serialNum;

    /**
     * Local date field for storing part manufacture date
     */
    @Column(name = "DateManufactured")
    @NotNull(message = "Please provide manufacture date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateManufactured;

    /**
     * To provide meaningful printout in log default "toString" method is overridden
     *
     * @return String
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

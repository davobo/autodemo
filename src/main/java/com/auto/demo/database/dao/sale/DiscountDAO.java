package com.auto.demo.database.dao.sale;

import com.auto.demo.database.model.sale.Discount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Discount repository interface
 */
@Repository
public interface DiscountDAO extends JpaRepository<Discount, Integer> {

    /**
     * Fetch discount by start date
     *
     * @param pageable Pageable
     * @param start    LocalDate
     * @return Page<Discount>
     */
    Page<Discount> findByStart(Pageable pageable, LocalDate start);

    /**
     * Fetch discount by end date
     *
     * @param pageable Pageable
     * @param end      LocalDate
     * @return Page<Discount>
     */
    Page<Discount> findByEnd(Pageable pageable, LocalDate end);

    /**
     * Fetch discount by article Id
     *
     * @param articleId Integer
     * @return Optional<Discount>
     */
    @Query(value = "SELECT * FROM DISCOUNT where articleId=:articleId", nativeQuery = true)
    Optional<Discount> findByArticleId(@Param("articleId") Integer articleId);

    /**
     * Delete discount based on article Id
     *
     * @param articleId Integer
     */
    @Modifying
    @Query(value = "DELETE FROM DISCOUNT WHERE articleId = :articleId", nativeQuery = true)
    void deleteByArticle(@Param("articleId") Integer articleId);
}

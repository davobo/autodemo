package com.auto.demo.database.dao.sale;

import com.auto.demo.database.model.sale.Article;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Article repository interface
 */
@Repository
public interface ArticleDAO extends JpaRepository<Article, Integer> {

    /**
     * Fetch article by part Id
     *
     * @param partId Integer
     * @return Optional<Article>
     */
    @Query(value = "SELECT a.articleid, a.partserial, a.price FROM ARTICLE as a" +
            " LEFT JOIN part on part.serialnum=a.partserial where part.partid=:partId", nativeQuery = true)
    Optional<Article> findByPartId(@Param("partId") Integer partId);

    /**
     * Fetch article by part serial number
     *
     * @param partSerial partSerial
     * @return Optional<Article>
     */
    @Query(value = "SELECT * FROM ARTICLE where partserial=:partSerial", nativeQuery = true)
    Optional<Article> findByPartSerial(@Param("partSerial") Integer partSerial);

    /**
     * Fetch all articles as offer with part serial, date manufactured and shopping price
     *
     * @param pageable Pageable
     * @return Optional<Article>
     */
    @Query(value = "SELECT partserial, datemanufactured, price, ISNULL(percent, 0) as percent FROM ARTICLE LEFT JOIN" +
            " part ON part.serialnum = article.partserial LEFT JOIN discount ON discount.articleid = article.articleid",
            nativeQuery = true)
    List<offerInterface> getOffer(Pageable pageable);

    /**
     * Interface for projection to Offer object
     */
    interface offerInterface {
        @Value("#{target.partserial}")
        Integer getPartSerial();

        @Value("#{target.datemanufactured}")
        Date getDateManufactured();

        @Value("#{(target.price*100)/(target.percent+100)}")
        BigDecimal getPrice();
    }

}

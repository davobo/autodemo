package com.auto.demo.database.dao.warehouse;

import com.auto.demo.database.model.warehouse.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Brand repository interface
 */
@Repository
public interface BrandDAO extends JpaRepository<Brand, Integer> {

    /**
     * Fetch brands by name
     *
     * @param name String
     * @return Optional<Brand>
     */
    Optional<Brand> findByNameIgnoreCase(String name);
}

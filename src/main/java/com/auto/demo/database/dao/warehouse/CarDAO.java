package com.auto.demo.database.dao.warehouse;

import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.database.model.warehouse.CarAndBrand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Car repository interface
 */
@Repository
public interface CarDAO extends JpaRepository<Car, Integer> {

    /**
     * Fetch car by name
     *
     * @param name String
     * @return Optional<Car>
     */
    Page<Car> findByNameIgnoreCase(Pageable pageable, String name);

    /**
     * Method used for fetching number of parts based on car and brand name
     *
     * @param pageable Pageable
     * @return Page<CarAndBrand>
     */
    @Query("SELECT new com.auto.demo.database.model.warehouse.CarAndBrand(c.name, b.name, count(c.part)) FROM Car as c LEFT JOIN" +
            " Brand as b on c.brand=b.brandId GROUP BY c.name, b.name")
    Page<CarAndBrand> getParts(Pageable pageable);

    /**
     * Method used for fetching number of parts based on provided car and brand name
     *
     * @param car   String
     * @param brand String
     * @return CarAndBrand
     */
    @Query("SELECT new com.auto.demo.database.model.warehouse.CarAndBrand(c.name, b.name, count(c.part)) FROM Car as c LEFT JOIN" +
            " Brand as b on c.brand=b.brandId WHERE c.name = :car AND b.name = :brand")
    Optional<CarAndBrand> getPartsForCarAndBrand(@Param("car") String car, @Param("brand") String brand);

    /**
     * Method used for deleting all cars based on part Id
     *
     * @param partId Integer
     */
    @Modifying
    @Query(value = "DELETE FROM car WHERE part = :partId", nativeQuery = true)
    void deleteByPart(@Param("partId") Integer partId);

    /**
     * Method used for deleting all cars based on brand Id
     *
     * @param brandId Integer
     */
    @Modifying
    @Query(value = "DELETE FROM car WHERE brand = :brandId", nativeQuery = true)
    void deleteByBrand(@Param("brandId") Integer brandId);
}

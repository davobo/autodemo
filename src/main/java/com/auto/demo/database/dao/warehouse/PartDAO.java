package com.auto.demo.database.dao.warehouse;

import com.auto.demo.database.model.warehouse.Part;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Part repository interface
 */
@Repository
public interface PartDAO extends JpaRepository<Part, Integer> {

    /**
     * Method used for fetching parts based on part name
     *
     * @param name String
     * @return Optional<Part>
     */
    Optional<Part> findByNameIgnoreCase(String name);

    /**
     * Method used for fetching parts based on part serial number
     *
     * @param serialNumber Integer
     * @return Optional<Part>
     */
    Optional<Part> findBySerialNum(Integer serialNumber);

    /**
     * Method used for fetching parts based on date manufactured
     *
     * @param date     LocalDate
     * @param pageable Pageable
     * @return Optional<Page < Part>>
     */
    Optional<Page<Part>> findByDateManufactured(LocalDate date, Pageable pageable);
}

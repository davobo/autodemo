package com.auto.demo.controller;

import com.auto.demo.exception.ErrorResponse;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * TODO: Handle different errors 404, 500 ...
 */
@Controller
public class CustomErrorController extends AbstractErrorController {

    public CustomErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    /**
     * Rest interface to handle "/error"
     *
     * @param request HttpServletRequest
     * @return ResponseEntity<ErrorResponse>
     */
    @RequestMapping("/error")
    public final ResponseEntity<ErrorResponse> handleUrlNotFoundException(HttpServletRequest request) {
        int indexOf = request.getRequestURL().indexOf("/error");
        ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND.value(),
                "URL: " + request.getRequestURL().delete(indexOf, indexOf + "/error".length())
                        + super.getErrorAttributes(request, ErrorAttributeOptions.defaults()).get("path").toString()
                        + " not found");
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @Override
    public String getErrorPath() {
        return null;
    }
}

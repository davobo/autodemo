package com.auto.demo.controller.warehouse;

import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.service.warehouse.PartService;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Part rest controller
 */
@RestController
@RequestMapping("/api/warehouse/part")
@Slf4j
public class PartController {

    /**
     * Service used to perform CRUD operations on "part" table
     */
    private final PartService partService;

    /**
     * Constructor for CarController class
     *
     * @param partService PartService
     */
    public PartController(PartService partService) {
        this.partService = partService;
    }

    /**
     * Rest interface for fetching all parts in DB
     *
     * @param pageable Pageable
     * @return PagedModel<EntityModel < Part>>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "partId,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get all parts")
    @GetMapping
    public PagedModel<EntityModel<Part>> findAll(@Parameter(hidden = true) Pageable pageable,
                                                 PagedResourcesAssembler<Part> assembler) {
        log.info("Fetching all parts");
        Page<Part> pageParts = partService.findAll(pageable);
        pageParts.forEach(part -> {
            part.add(linkTo(methodOn(PartController.class)
                    .findById(part.getPartId())).withSelfRel());
        });
        return assembler.toModel(pageParts);
    }

    /**
     * Rest interface to fetch specific part from DB by id
     *
     * @param partId Integer
     * @return EntityModel<Part>
     */
    @Operation(summary = "Get parts by ID")
    @GetMapping("/{partId}")
    public RepresentationModel<Part> findById(@PathVariable Integer partId) {
        log.info("Fetching part with ID: " + partId);
        Part part = partService.findById(partId);
        return HalModelBuilder.emptyHalModel().embed(part).link(linkTo(methodOn(PartController.class)
                .findById(part.getPartId())).withSelfRel()).build();
    }

    /**
     * Rest interface to fetch specific part from DB by serial number
     *
     * @param serialNumber Integer
     * @return EntityModel<Part>
     */
    @Operation(summary = "Get part by serial number")
    @GetMapping("/serial/{serialNumber}")
    public RepresentationModel<Part> findBySerialNumber(@PathVariable Integer serialNumber) {
        log.info("Fetching part with serial number: " + serialNumber);
        Part part = partService.findBySerialNumber(serialNumber);
        return HalModelBuilder.emptyHalModel().embed(part).link(linkTo(methodOn(PartController.class)
                .findById(part.getPartId())).withSelfRel()).build();
    }

    /**
     * Rest interface to fetching parts from DB by date
     *
     * @param date Date
     * @return PagedModel<EntityModel < Part>>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "dateManufactured,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get parts by manufacturing date")
    @GetMapping("/date")
    public PagedModel<EntityModel<Part>> findByDate(
            @RequestParam("date") @JsonFormat(pattern = "yyyy-MM-dd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            @Parameter(hidden = true) Pageable pageable,
            PagedResourcesAssembler<Part> assembler) {
        log.info("Fetching parts with date: " + date);
        Page<Part> pageParts = partService.findByDate(date, pageable);
        pageParts.forEach(part -> {
            part.add(linkTo(methodOn(PartController.class)
                    .findById(part.getPartId())).withSelfRel());
        });
        return assembler.toModel(pageParts, linkTo(methodOn(PartController.class).
                findByDate(date, pageable, assembler)).withSelfRel());
    }

    /**
     * Rest interface to create new part
     *
     * @param part Part
     * @return Part
     */
    @Operation(summary = "Create part")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public RepresentationModel<Part> create(@Valid @RequestBody Part part) {
        log.info("Creating part \"" + part.getName() + "\"");
        Part newPart = partService.create(part);
        return HalModelBuilder.emptyHalModel().embed(newPart).link(linkTo(methodOn(PartController.class)
                .findById(newPart.getPartId())).withSelfRel()).build();
    }

    /**
     * Rest interface to update part based on ID
     *
     * @param partId      Integer
     * @param partRequest Part
     */
    @Operation(summary = "Update part by ID")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{partId}")
    public void update(@PathVariable Integer partId, @RequestBody Part partRequest) {
        log.info("Updating part with ID:" + partId + " to: " + partRequest.toString());
        partService.update(partId, partRequest);
    }

    /**
     * Rest interface to delete part by provided ID
     *
     * @param partId Integer
     */
    @Operation(summary = "Delete part by ID")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{partId}")
    public void delete(@PathVariable Integer partId) {
        log.info("Deleting part with ID:" + partId);
        partService.delete(partId);
    }
}

package com.auto.demo.controller.warehouse.serializer;

import com.auto.demo.database.model.warehouse.CarAndBrand;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Custom serializer to provide requested format
 */
@Component
public class PartsSerializer extends JsonSerializer<CarAndBrand> {

    /**
     * Method used to perform serialization
     *
     * @param carAndBrand        CarAndBrand
     * @param jsonGenerator      JsonGenerator
     * @param serializerProvider SerializerProvider
     * @throws IOException Exception if there is a problem in serialization
     */
    @Override
    public void serialize(CarAndBrand carAndBrand, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {
        if (StringUtils.isEmpty(jsonGenerator.getOutputContext().getCurrentName()))
            jsonGenerator.writeFieldName("CarAndBrand");
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("brand_and_automobile", carAndBrand.getBrand() + " " + carAndBrand.getCar());
        jsonGenerator.writeNumberField("count", carAndBrand.getParts());
        jsonGenerator.writeEndObject();
    }
}

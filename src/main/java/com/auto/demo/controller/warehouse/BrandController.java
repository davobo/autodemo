package com.auto.demo.controller.warehouse;

import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.service.warehouse.BrandService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Brand rest controller
 */
@Slf4j
@RestController
@RequestMapping("/api/warehouse/brand")
public class BrandController {

    /**
     * Service used to perform CRUD operations on "brand" table
     */
    private final BrandService brandService;

    /**
     * Constructor for BrandController class
     *
     * @param brandService BrandService
     */
    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    /**
     * Rest interface for fetching all brands in DB
     *
     * @param pageable Pageable
     * @return PagedModel<EntityModel < Brand>>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "brandId,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get all brands")
    @GetMapping
    public PagedModel<EntityModel<Brand>> findAll(@Parameter(hidden = true) Pageable pageable, PagedResourcesAssembler<Brand> assembler) {
        log.info("Fetching all brands");
        Page<Brand> pageBrand = brandService.findAll(pageable);
        pageBrand.forEach(brand -> {
            brand.add(linkTo(methodOn(BrandController.class)
                    .findById(brand.getBrandId())).withSelfRel());
        });
        return assembler.toModel(pageBrand);
    }

    /**
     * Rest interface to fetch specific brand from DB by id
     *
     * @param brandId Integer
     * @return EntityModel<Brand>
     */
    @Operation(summary = "Get brand by ID")
    @GetMapping("/{brandId}")
    public RepresentationModel<Brand> findById(@PathVariable Integer brandId) {
        log.info("Fetching brand with ID: {}.", brandId);
        Brand brand = brandService.findById(brandId);
        return HalModelBuilder.emptyHalModel().embed(brand).link(linkTo(methodOn(BrandController.class)
                .findById(brandId)).withSelfRel()).build();
    }

    /**
     * Rest interface to fetch specific brand from DB by name
     *
     * @param name String
     * @return EntityModel<Brand>
     */
    @Operation(summary = "Get brand by name")
    @GetMapping("/name/{name}")
    public RepresentationModel<Brand> findByName(@PathVariable String name) {
        log.info("Fetching brand with name: {}.", name);
        Brand brand = brandService.findByName(name);
        return HalModelBuilder.emptyHalModel().embed(brand).link(linkTo(methodOn(BrandController.class)
                .findByName(name)).withSelfRel()).build();
    }

    /**
     * Rest interface to create new brand
     *
     * @param brand Brand
     * @return Part
     */
    @Operation(summary = "Create brand")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public RepresentationModel<Brand> create(@Valid @RequestBody Brand brand) {
        log.info("Creating brand {}.", brand.getName());
        Brand newBrand = brandService.create(brand);
        return HalModelBuilder.emptyHalModel().embed(newBrand).link(linkTo(methodOn(BrandController.class)
                .findById(newBrand.getBrandId())).withSelfRel()).build();
    }

    /**
     * Rest interface to update brand
     *
     * @param brandId      Integer
     * @param brandRequest Brand
     */
    @Operation(summary = "Update brand by ID")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{brandId}")
    public void update(@PathVariable Integer brandId, @Valid @RequestBody Brand brandRequest) {
        log.info("Updating brand with ID: {} to: {}.", brandId, brandRequest.toString());
        brandService.update(brandId, brandRequest);
    }

    /**
     * Rest interface to delete brand
     *
     * @param brandId Integer
     */
    @Operation(summary = "Delete brand")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{brandId}")
    public void delete(@PathVariable Integer brandId) {
        log.info("Deleting brand with ID: {}.", brandId);
        brandService.delete(brandId);
    }
}

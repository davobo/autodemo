package com.auto.demo.controller.warehouse;

import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.database.model.warehouse.CarAndBrand;
import com.auto.demo.service.warehouse.CarService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.ConstraintViolationException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Car rest controller
 */
@Slf4j
@RestController
@RequestMapping("/api/warehouse/car")
public class CarController {

    /**
     * Service used to perform CRUD operations on "car" table
     */
    private final CarService carService;

    /**
     * Constructor for CarController class
     *
     * @param carService CarService
     */
    public CarController(CarService carService) {
        this.carService = carService;
    }

    /**
     * Rest interface for fetching all cars in DB
     *
     * @param pageable Pageable
     * @return PagedModel<EntityModel < Car>>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "carId,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get all cars")
    @GetMapping
    public PagedModel<EntityModel<Car>> findAll(@Parameter(hidden = true) Pageable pageable,
                                                PagedResourcesAssembler<Car> assembler) {
        log.info("Fetching all cars");
        Page<Car> carBrand = carService.findAll(pageable);
        carBrand.forEach(car -> {
            car.add(linkTo(methodOn(CarController.class)
                    .findById(car.getCarId())).withSelfRel());
        });
        return assembler.toModel(carBrand);
    }

    /**
     * Rest interface to fetch specific car from DB by id
     *
     * @param carId Integer
     * @return EntityModel<Car>
     */
    @Operation(summary = "Get car by ID")
    @GetMapping("/{carId}")
    public RepresentationModel<Car> findById(@PathVariable Integer carId) {
        log.info("Fetching car with ID: {}.", carId);
        Car car = carService.findById(carId);
        car.getPart().add(linkTo(methodOn(PartController.class)
                .findById(car.getPart().getPartId())).withSelfRel());
        car.getBrand().add(linkTo(methodOn(BrandController.class)
                .findById(car.getBrand().getBrandId())).withSelfRel());
        return HalModelBuilder.emptyHalModel().embed(car).link(linkTo(methodOn(CarController.class)
                .findById(carId)).withSelfRel()).build();
    }

    /**
     * Rest interface to fetch specific car from DB by name
     *
     * @param name String
     * @return EntityModel<Car>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "name,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get car by name")
    @GetMapping("/name/{name}")
    public PagedModel<EntityModel<Car>> findByName(
            @Parameter(hidden = true) Pageable pageable,
            @PathVariable String name, PagedResourcesAssembler<Car> assembler) {
        log.info("Fetching brand with name: {}.", name);
        Page<Car> carPage = carService.findByName(pageable, name);
        carPage.forEach(car -> {
            car.add(linkTo(methodOn(CarController.class)
                    .findByName(pageable, name, assembler)).withSelfRel());
        });
        return assembler.toModel(carPage);
    }

    /**
     * Rest interface to create new car
     *
     * @param car Car
     * @return Car
     */
    @Operation(summary = "Create car")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/brand/{brandId}/part/{partId}")
    public RepresentationModel<Car> create(@PathVariable Integer brandId, @PathVariable Integer partId, @RequestBody Car car) {
        log.info("Creating car {}.", car.getName());
        Car newCar;
        try {
            newCar = carService.create(brandId, partId, car);
        } catch (ConstraintViolationException cve) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, cve.getMessage());
        }
        newCar.getPart().add(linkTo(methodOn(PartController.class)
                .findById(newCar.getPart().getPartId())).withSelfRel());
        newCar.getBrand().add(linkTo(methodOn(BrandController.class)
                .findById(newCar.getBrand().getBrandId())).withSelfRel());
        return HalModelBuilder.emptyHalModel().embed(newCar).link(linkTo(methodOn(CarController.class)
                .findById(newCar.getCarId())).withSelfRel()).build();
    }

    /**
     * Rest interface to update car
     *
     * @param carId      Integer
     * @param carRequest Car
     */
    @Operation(summary = "Update car by ID")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{carId}")
    public void update(@PathVariable Integer carId, @RequestBody Car carRequest) {
        log.info("Updating car with ID: {} to: {}.", carId, carRequest.toString());
        carService.update(carId, carRequest);
    }

    /**
     * Rest interface to delete car
     *
     * @param carId Integer
     */
    @Operation(summary = "Delete car")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{carId}")
    public void delete(@PathVariable Integer carId) {
        log.info("Deleting brand with ID:" + carId);
        carService.delete(carId);
    }

    /**
     * Rest interface to fetch parts by car and brand from car table
     *
     * @param pageable  Pageable
     * @param assembler PagedResourcesAssembler<CarAndBrand>
     * @return PagedModel<EntityModel < CarAndBrand>>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "part,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get all parts")
    @GetMapping("/parts")
    public PagedModel<EntityModel<CarAndBrand>> getParts(@Parameter(hidden = true) Pageable pageable,
                                                         PagedResourcesAssembler<CarAndBrand> assembler) {
        log.info("Fetching parts by all cars and brands.");
        Page<CarAndBrand> carAndBrand = carService.getParts(pageable);
        carAndBrand.forEach(car -> {
            car.add(linkTo(methodOn(CarController.class)
                    .getPartsByCarAndBrand(car.getCar(), car.getBrand())).withSelfRel());
        });
        return assembler.toModel(carAndBrand);
    }

    /**
     * Rest interface to fetch parts by car and brand from car table by car and brand name
     *
     * @param car   String
     * @param brand String
     * @return RepresentationModel<CarAndBrand>
     */
    @Operation(summary = "Get parts by car and brand")
    @GetMapping("/partsByCarAndBrand")
    public RepresentationModel<CarAndBrand> getPartsByCarAndBrand(@RequestParam("car") String car,
                                                                  @RequestParam("brand") String brand) {
        log.info("Fetching parts by car and brand ");
        CarAndBrand carAndBrand = carService.getPartsByCarAndBrand(car, brand);
        return HalModelBuilder.emptyHalModel().embed(carAndBrand).link(linkTo(methodOn(CarController.class)
                .getPartsByCarAndBrand(car, brand)).withSelfRel()).build();
    }
}

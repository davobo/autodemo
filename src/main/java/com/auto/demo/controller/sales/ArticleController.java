package com.auto.demo.controller.sales;

import com.auto.demo.controller.warehouse.PartController;
import com.auto.demo.database.model.sale.Article;
import com.auto.demo.database.model.sale.Offer;
import com.auto.demo.service.sales.ArticleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.ConstraintViolationException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Article rest controller
 */
@Slf4j
@RestController
@RequestMapping("/api/sale/article")
public class ArticleController {

    /**
     * Service used to perform CRUD operations on "article" table
     */
    private final ArticleService articleService;

    /**
     * Constructor for ArticleController class
     *
     * @param articleService ArticleService
     */
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    /**
     * Rest interface for fetching all articles in DB
     *
     * @param pageable Pageable
     * @return PagedModel<EntityModel < Article>>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "articleId,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get all articles")
    @GetMapping
    public PagedModel<EntityModel<Article>> findAll(@Parameter(hidden = true) Pageable pageable, PagedResourcesAssembler<Article> assembler) {
        log.info("Fetching all articles");
        Page<Article> pageArticles = articleService.findAll(pageable);
        pageArticles.forEach(article -> {
            article.getPartSerial().add(linkTo(methodOn(PartController.class)
                    .findById(article.getPartSerial().getPartId())).withSelfRel());
            article.add(linkTo(methodOn(ArticleController.class)
                    .findById(article.getArticleId())).withSelfRel());
        });
        return assembler.toModel(pageArticles);
    }

    /**
     * Rest interface to fetch specific article from DB by id
     *
     * @param articleId Integer
     * @return EntityModel<Article>
     */
    @Operation(summary = "Get article by ID")
    @GetMapping(path = "/{articleId}")
    public RepresentationModel<Article> findById(@PathVariable Integer articleId) {
        log.info("Fetching article with ID: {}", articleId);
        Article article = articleService.findById(articleId);
        article.getPartSerial().add(linkTo(methodOn(PartController.class)
                .findById(article.getPartSerial().getPartId())).withSelfRel());
        return HalModelBuilder.emptyHalModel().embed(article).link(linkTo(methodOn(ArticleController.class)
                .findById(articleId)).withSelfRel()).build();
    }

    /**
     * Rest interface to fetch specific article from DB by serial number
     *
     * @param serialNumber Integer
     * @return EntityModel<Article>
     */
    @Operation(summary = "Get article by product serial number")
    @GetMapping("/partSerial/{serialNumber}")
    public RepresentationModel<Article> findByPartSerial(@PathVariable Integer serialNumber) {
        log.info("Fetching article by part serial number: {}", serialNumber);
        Article article = articleService.findByPartSerial(serialNumber);
        article.getPartSerial().add(linkTo(methodOn(PartController.class)
                .findById(article.getPartSerial().getPartId())).withSelfRel());
        return HalModelBuilder.emptyHalModel().embed(article).link(linkTo(methodOn(ArticleController.class)
                .findByPartSerial(serialNumber)).withSelfRel()).build();
    }

    /**
     * Rest interface to fetch specific entire offer
     *
     * @return EntityModel<Article>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "partSerial,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get offers")
    @GetMapping("/offer")
    public PagedModel<EntityModel<Offer>> getOffer(@Parameter(hidden = true) Pageable pageable, PagedResourcesAssembler<Offer> assembler) {
        log.info("Fetching articles for offer");
        Page<Offer> pagedOffer = articleService.getOffer(pageable);
        pagedOffer.forEach(offer -> {
            offer.add(linkTo(methodOn(ArticleController.class)
                    .findByPartSerial(offer.getPartSerial())).withSelfRel());
        });
        return assembler.toModel(pagedOffer);
    }

    /**
     * Rest interface to create new Article
     *
     * @param article Article
     * @return Article
     */
    @Operation(summary = "Create articles")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/partSerial/{partSerial}")
    public RepresentationModel<Article> create(@PathVariable Integer partSerial, @RequestBody Article article) {
        log.info("Creating article with serial number: {} and price: {}.", partSerial, article.getPrice());
        Article newArticle;
        try {
            newArticle = articleService.create(partSerial, article);
        } catch (ConstraintViolationException cve) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, cve.getMessage());
        }
        newArticle.getPartSerial().add(linkTo(methodOn(PartController.class)
                .findById(newArticle.getPartSerial().getPartId())).withSelfRel());
        return HalModelBuilder.emptyHalModel().embed(newArticle).link(linkTo(methodOn(ArticleController.class)
                .findById(newArticle.getArticleId())).withSelfRel()).build();
    }

    /**
     * Rest interface to update article under specified part serial
     *
     * @param partId         Integer
     * @param articleRequest Article
     */
    @Operation(summary = "Update article by part serial number")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("part/{partId}")
    public void updateByPartSerial(@PathVariable Integer partId, @RequestBody Article articleRequest) {
        log.info("Updating article with part: {} to: {}.", partId, articleRequest.toString());
        articleService.updateByPartSerial(partId, articleRequest);
    }

    /**
     * Rest interface to update article under specified ID
     *
     * @param articleId      Integer
     * @param articleRequest Article
     */
    @Operation(summary = "Update article by ID")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("{articleId}")
    public void update(@PathVariable Integer articleId, @RequestBody Article articleRequest) {
        log.info("Updating article with part: {} to: {}.", articleId, articleRequest.toString());
        articleService.update(articleId, articleRequest);
    }

    /**
     * Rest interface to delete article
     *
     * @param articleId Integer
     */
    @Operation(summary = "Delete article by ID")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{articleId}")
    public void delete(@PathVariable Integer articleId) {
        log.info("Deleting article with ID: {}.", articleId);
        articleService.delete(articleId);
    }
}

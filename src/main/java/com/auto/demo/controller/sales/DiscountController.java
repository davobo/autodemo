package com.auto.demo.controller.sales;

import com.auto.demo.controller.warehouse.PartController;
import com.auto.demo.database.model.sale.Discount;
import com.auto.demo.service.sales.DiscountService;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Discount rest controller
 */
@Slf4j
@RestController
@RequestMapping("/api/sale/discount")
public class DiscountController {

    /**
     * Service used to perform CRUD operations on "discount" table
     */
    private final DiscountService discountService;

    /**
     * Constructor for DiscountController class
     *
     * @param discountService DiscountService
     */
    public DiscountController(DiscountService discountService) {
        this.discountService = discountService;
    }

    /**
     * Rest interface for fetching all discounts in DB
     *
     * @param pageable Pageable
     * @return PagedModel<EntityModel < discount>>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "discountId,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get all discounts")
    @GetMapping
    public PagedModel<EntityModel<Discount>> findAll(@Parameter(hidden = true) Pageable pageable, PagedResourcesAssembler<Discount> assembler) {
        log.info("Fetching all discounts");
        Page<Discount> pageDiscounts = discountService.findAll(pageable);
        pageDiscounts.forEach(discount -> {
            discount.getArticle().getPartSerial().add(linkTo(methodOn(PartController.class)
                    .findById(discount.getArticle().getPartSerial().getPartId())).withSelfRel());
            discount.getArticle().add(linkTo(methodOn(ArticleController.class)
                    .findById(discount.getArticle().getArticleId())).withSelfRel());
            discount.add(linkTo(methodOn(DiscountController.class)
                    .findById(discount.getDiscountId())).withSelfRel());
        });
        return assembler.toModel(pageDiscounts);
    }

    /**
     * Rest interface to fetch specific discount from DB by id
     *
     * @param discountId Integer
     * @return EntityModel<Discount>
     */
    @Operation(summary = "Get discount by ID")
    @GetMapping("/{discountId}")
    public RepresentationModel<Discount> findById(@PathVariable Integer discountId) {
        log.info("Fetching discount with ID: {}.", discountId);
        Discount discount = discountService.findById(discountId);
        discount.getArticle().getPartSerial().add(linkTo(methodOn(PartController.class)
                .findById(discount.getArticle().getPartSerial().getPartId())).withSelfRel());
        discount.getArticle().add(linkTo(methodOn(ArticleController.class)
                .findById(discount.getArticle().getArticleId())).withSelfRel());
        return HalModelBuilder.emptyHalModel().embed(discount).link(linkTo(methodOn(DiscountController.class)
                .findById(discountId)).withSelfRel()).build();
    }

    /**
     * Rest interface to fetch specific discount from DB by stat date
     *
     * @param pageable  Pageable
     * @param start     LocalDate
     * @param assembler PagedResourcesAssembler<Discount>
     * @return EntityModel<Discount>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "start,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get discount by start date")
    @GetMapping("/start")
    public PagedModel<EntityModel<Discount>> findByStart(
            @Parameter(hidden = true) Pageable pageable,
            @RequestParam("date") @JsonFormat(pattern = "yyyy-MM-dd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start,
            PagedResourcesAssembler<Discount> assembler) {
        log.info("Fetching discount by start date: {}", start);
        Page<Discount> discountPage = discountService.findByStart(pageable, start);
        discountPage.forEach(discount -> {
            discount.add(linkTo(methodOn(DiscountController.class)
                    .findById(discount.getDiscountId())).withSelfRel());
            discount.getArticle().add(linkTo(methodOn(ArticleController.class)
                    .findById(discount.getArticle().getArticleId())).withSelfRel());
            discount.getArticle().getPartSerial().add(linkTo(methodOn(PartController.class)
                    .findById(discount.getArticle().getPartSerial().getPartId())).withSelfRel());
        });
        return assembler.toModel(discountPage, linkTo(methodOn(DiscountController.class).
                findByStart(pageable, start, assembler)).withSelfRel());
    }

    /**
     * Rest interface to fetch specific discount from DB by end date
     *
     * @param pageable  Pageable
     * @param end       LocalDate
     * @param assembler PagedResourcesAssembler<Discount>
     * @return EntityModel<Discount>
     */
    @Parameters({
            @Parameter(name = "page", schema = @Schema(type = "integer", defaultValue = "0"), in = ParameterIn.QUERY,
                    description = "Results page you want to retrieve (0..N)"),
            @Parameter(name = "size", schema = @Schema(type = "integer", defaultValue = "5"), in = ParameterIn.QUERY,
                    description = "Number of records per page."),
            @Parameter(name = "sort", schema = @Schema(type = "string", example = "end,desc"), in = ParameterIn.QUERY,
                    description = "Sorting criteria in the format: property,(asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @Operation(summary = "Get discount by end date")
    @GetMapping("/end")
    public PagedModel<EntityModel<Discount>> findByEnd(
            @Parameter(hidden = true) Pageable pageable,
            @RequestParam("date") @JsonFormat(pattern = "yyyy-MM-dd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end,
            PagedResourcesAssembler<Discount> assembler) {
        log.info("Fetching discount by end date: {}.", end);
        Page<Discount> discountPage = discountService.findByEnd(pageable, end);
        discountPage.forEach(discount -> {
            discount.add(linkTo(methodOn(DiscountController.class)
                    .findById(discount.getDiscountId())).withSelfRel());
            discount.getArticle().add(linkTo(methodOn(ArticleController.class)
                    .findById(discount.getArticle().getArticleId())).withSelfRel());
            discount.getArticle().getPartSerial().add(linkTo(methodOn(PartController.class)
                    .findById(discount.getArticle().getPartSerial().getPartId())).withSelfRel());
        });
        return assembler.toModel(discountPage, linkTo(methodOn(DiscountController.class).
                findByEnd(pageable, end, assembler)).withSelfRel());
    }

    /**
     * Rest interface to fetch specific discount from DB by article id
     *
     * @param articleId Integer
     * @return EntityModel<Discount>
     */
    @Operation(summary = "Get discount by article ID")
    @GetMapping("/article/{articleId}")
    public RepresentationModel<Discount> findByArticleId(@PathVariable Integer articleId) {
        log.info("Fetching discount with article Id: {}.", articleId);
        Discount discount = discountService.findByArticleId(articleId);
        return HalModelBuilder.emptyHalModel().embed(discount).link(linkTo(methodOn(DiscountController.class)
                .findByArticleId(articleId)).withSelfRel()).build();
    }

    /**
     * Rest interface to create new discount
     *
     * @param articleId Integer
     * @param discount  Discount
     * @return Article
     */
    @Operation(summary = "Create discount")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/article/{articleId}")
    public RepresentationModel<Discount> create(@PathVariable Integer articleId, @RequestBody Discount discount) {
        log.info("Creating discount {}.", discount.toString());
        Discount newDiscount;
        try {
            newDiscount = discountService.create(articleId, discount);
        } catch (ConstraintViolationException cve) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, cve.getMessage());
        }
        return HalModelBuilder.emptyHalModel().embed(newDiscount).link(linkTo(methodOn(DiscountController.class)
                .findById(newDiscount.getDiscountId())).withSelfRel()).build();
    }

    /**
     * Rest interface to update discount under specified ID
     *
     * @param articleId       Integer
     * @param discountRequest Discount
     */
    @Operation(summary = "Update discount by ID")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/article/{articleId}")
    public void updateByArticleId(@PathVariable Integer articleId, @RequestBody Discount discountRequest) {
        log.info("Updating discount with ID: {} to: {}.", articleId, discountRequest.toString());
        discountService.updateByArticleId(articleId, discountRequest);
    }

    /**
     * Rest interface to update discount under specified ID
     *
     * @param discountId      Integer
     * @param discountRequest Discount
     */
    @Operation(summary = "Update discount by ID")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("{discountId}")
    public void update(@PathVariable Integer discountId, @RequestBody Discount discountRequest) {
        log.info("Updating discount with ID: {} to: {}.", discountId, discountRequest.toString());
        discountService.update(discountId, discountRequest);
    }

    /**
     * Rest interface to delete discount
     *
     * @param discountId Integer
     */
    @Operation(summary = "Delete discount")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{discountId}")
    public void delete(@PathVariable Integer discountId) {
        log.info("Deleting discount with ID: {}.", discountId);
        discountService.delete(discountId);
    }
}

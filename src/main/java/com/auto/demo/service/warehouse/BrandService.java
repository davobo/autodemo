package com.auto.demo.service.warehouse;

import com.auto.demo.database.model.warehouse.Brand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface BrandService {

    /**
     * Interface for fetching all brands from "brand" table
     *
     * @param pageable Pageable
     * @return Page<Brand>
     */
    Page<Brand> findAll(Pageable pageable);

    /**
     * Interface for fetching individual brand from "brand" table based on its ID
     *
     * @param id Integer
     * @return Brand
     */
    Brand findById(Integer id);

    /**
     * Interface for fetching individual brand from "brand" table based on its name
     *
     * @param name String
     * @return Brand
     */
    Brand findByName(String name);

    /**
     * Interface to store new brand in "brand" table
     *
     * @param brand Brand
     * @return Brand
     */
    Brand create(Brand brand);

    /**
     * Interface to update existing brand
     *
     * @param brandId      Integer
     * @param brandRequest Brand
     * @return Brand
     */
    Brand update(Integer brandId, Brand brandRequest);

    /**
     * Interface to remove brand from "brand" table by provided id
     *
     * @param brandId Integer
     * @return ResponseEntity<?>
     */
    void delete(Integer brandId);
}

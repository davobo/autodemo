package com.auto.demo.service.warehouse;

import com.auto.demo.common.util.CustomBeanUtils;
import com.auto.demo.database.dao.warehouse.CarDAO;
import com.auto.demo.database.dao.warehouse.PartDAO;
import com.auto.demo.database.model.warehouse.Part;
import com.auto.demo.exception.ResourceNotFoundException;
import com.auto.demo.service.sales.ArticleService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Service
public class PartServiceImpl implements PartService {

    /**
     * Object used to perform CRUD operations on "part" table
     */
    private final PartDAO partDAO;

    /**
     * Object used to perform CRUD operations on "car" table
     */
    private final CarDAO carDAO;

    /**
     * Service used to perform CRUD operations on "article" table
     */
    private final ArticleService articleService;

    public PartServiceImpl(PartDAO partDAO, CarDAO carDAO, ArticleService articleService) {
        this.partDAO = partDAO;
        this.carDAO = carDAO;
        this.articleService = articleService;
    }

    /**
     * Used for fetching all parts from "part" table
     *
     * @param pageable Pageable
     * @return Page<part>
     */
    @Override
    public Page<Part> findAll(Pageable pageable) {
        return partDAO.findAll(pageable);
    }

    /**
     * Used for fetching individual part from "part" table based on its ID
     *
     * @param id Integer
     * @return Part
     */
    @Transactional
    @Override
    public Part findById(Integer id) {
        return partDAO.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Part with id " + id + " not found"));
    }

    /**
     * Used for fetching individual part from "part" table based on its name
     *
     * @param name String
     * @return Part
     */
    @Override
    public Part findByName(String name) {
        return partDAO.findByNameIgnoreCase(name).orElseThrow(() ->
                new ResourceNotFoundException("Part with name " + name + " not found"));
    }

    /**
     * Used for fetching individual part from "part" table based on its serial number
     *
     * @param serialNumber Integer
     * @return Part
     */
    @Override
    public Part findBySerialNumber(Integer serialNumber) {
        return partDAO.findBySerialNum(serialNumber).orElseThrow(() ->
                new ResourceNotFoundException("Part with serial number " + serialNumber + " not found"));
    }

    /**
     * Used for fetching parts from "part" table based on manufactured date
     *
     * @param date LocalDate
     * @return Part
     */
    @Override
    public Page<Part> findByDate(LocalDate date, Pageable pageable) {
        return partDAO.findByDateManufactured(date, pageable).orElseThrow(() ->
                new ResourceNotFoundException("Part with manufacture date " + date + " not found"));
    }

    /**
     * Used to store new part in "part" table
     *
     * @param part Part
     * @return Part
     */
    @Transactional
    @Override
    public Part create(Part part) {
        return partDAO.save(part);
    }

    /**
     * Used to update existing part under specified id
     *
     * @param partId      Integer
     * @param partRequest Part
     * @return Part
     */
    @Transactional
    @Override
    public Part update(Integer partId, Part partRequest) {
        return partDAO.findById(partId).map(part -> {
            BeanUtils.copyProperties(partRequest, part, CustomBeanUtils.getNullPropertyNames(partRequest));
            return partDAO.save(part);
        }).orElseThrow(() ->
                new ResourceNotFoundException("Part with id " + partId + " not found"));
    }

    /**
     * Used to remove part from "part" table by provided id
     *
     * @param partId Integer
     * @return ResponseEntity<?>
     */
    @Transactional
    @Override
    public void delete(Integer partId) {
        // First remove FK constraints from article and car
        articleService.deleteByPartId(partId);
        carDAO.deleteByPart(partId);
        partDAO.deleteById(partId);
    }
}

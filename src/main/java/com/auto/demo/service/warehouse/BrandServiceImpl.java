package com.auto.demo.service.warehouse;

import com.auto.demo.database.dao.warehouse.BrandDAO;
import com.auto.demo.database.dao.warehouse.CarDAO;
import com.auto.demo.database.model.warehouse.Brand;
import com.auto.demo.exception.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BrandServiceImpl implements BrandService {

    /**
     * Object used to perform CRUD operations on "brand" table
     */
    private final BrandDAO brandDAO;

    /**
     * Object used to perform CRUD operations on "car" table
     */
    private final CarDAO carDAO;

    /**
     * Constructor for BrandController class
     *
     * @param brandDAO BrandDAO
     * @param carDAO   CarDAO
     */
    public BrandServiceImpl(BrandDAO brandDAO, CarDAO carDAO) {
        this.brandDAO = brandDAO;
        this.carDAO = carDAO;
    }

    /**
     * Used for fetching all brands from "brand" table
     *
     * @param pageable Pageable
     * @return Page<Brand>
     */
    @Override
    public Page<Brand> findAll(Pageable pageable) {
        return brandDAO.findAll(pageable);
    }

    /**
     * Used for fetching individual brand from "brand" table based on its ID
     *
     * @param id Integer
     * @return Brand
     */
    @Transactional
    @Override
    public Brand findById(Integer id) {
        return brandDAO.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Brand with id " + id + " not found"));
    }

    /**
     * Used for fetching individual brand from "brand" table based on its name
     *
     * @param name String
     * @return Brand
     */
    @Override
    public Brand findByName(String name) {
        return brandDAO.findByNameIgnoreCase(name).orElseThrow(() ->
                new ResourceNotFoundException("Brand with name " + name + " not found"));
    }

    /**
     * Used to store new brand in "brand" table
     *
     * @param brand Brand
     * @return Brand
     */
    @Transactional
    @Override
    public Brand create(Brand brand) {
        return brandDAO.save(brand);
    }

    /**
     * Used to update existing brand under specified id
     *
     * @param brandId      Integer
     * @param brandRequest Brand
     */
    @Transactional
    @Override
    public Brand update(Integer brandId, Brand brandRequest) {
        return brandDAO.findById(brandId).map(brand -> {
            brand.setName(brandRequest.getName());
            return brandDAO.save(brand);
        }).orElseThrow(() ->
                new ResourceNotFoundException("Brand with id " + brandId + " not found"));
    }

    /**
     * Used to remove brand from "brand" table by provided id
     *
     * @param brandId Integer
     */
    @Transactional
    @Override
    public void delete(Integer brandId) {
        // First FK related entities needs to be deleted
        carDAO.deleteByBrand(brandId);
        brandDAO.deleteById(brandId);
    }
}

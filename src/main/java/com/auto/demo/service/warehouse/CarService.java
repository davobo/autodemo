package com.auto.demo.service.warehouse;

import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.database.model.warehouse.CarAndBrand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface CarService {
    /**
     * Interface for fetching all cars from "car" table
     *
     * @param pageable Pageable
     * @return Page<Car>
     */
    Page<Car> findAll(Pageable pageable);

    /**
     * Interface for fetching individual car from "car" table based on its ID
     *
     * @param id Integer
     * @return Car
     */
    Car findById(Integer id);

    /**
     * Interface for fetching individual car from "car" table based on name
     *
     * @param pageable Pageable
     * @param name     String
     * @return Car
     */
    Page<Car> findByName(Pageable pageable, String name);

    /**
     * Interface to store new car in "car" table
     *
     * @param brandId Integer
     * @param partId  Integer
     * @param car     Car
     * @return Car
     */
    Car create(Integer brandId, Integer partId, Car car);

    /**
     * Interface to update existing car
     *
     * @param carId      Integer
     * @param carRequest Car
     * @return Car
     */
    Car update(Integer carId, Car carRequest);

    /**
     * Interface to remove car from "car" table by provided id
     *
     * @param carId Integer
     */
    void delete(Integer carId);

    /**
     * Interface to remove cars from "car" table by provided part Id
     *
     * @param partId Integer
     */
    void deleteByPart(Integer partId);

    /**
     * Interface to remove cars from "car" table by provided brand Id
     *
     * @param brandId Integer
     */
    void deleteByBrand(Integer brandId);

    /**
     * Interface for fetching all parts from "car" table by brand and car
     *
     * @param pageable Pageable
     * @return Page<CarAndBrand>
     */
    Page<CarAndBrand> getParts(Pageable pageable);

    /**
     * Interface for fetching all parts from "car" table by specified car and brand
     *
     * @param car   String
     * @param brand String
     * @return CarAndBrand
     */
    CarAndBrand getPartsByCarAndBrand(String car, String brand);
}

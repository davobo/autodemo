package com.auto.demo.service.warehouse;

import com.auto.demo.common.util.CustomBeanUtils;
import com.auto.demo.database.dao.warehouse.CarDAO;
import com.auto.demo.database.model.warehouse.Car;
import com.auto.demo.database.model.warehouse.CarAndBrand;
import com.auto.demo.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@AllArgsConstructor
@Service
public class CarServiceImpl implements CarService {

    /**
     * Object used to perform CRUD operations on "car" table
     */
    private CarDAO carDAO;

    /**
     * Service used to perform CRUD operations on "brand" table
     */
    private BrandService brandService;

    /**
     * Service used to perform CRUD operations on "part" table
     */
    private PartService partService;

    /**
     * Used for fetching all cars from "car" table
     *
     * @param pageable Pageable
     * @return Page<Car>
     */
    @Override
    public Page<Car> findAll(Pageable pageable) {
        return carDAO.findAll(pageable);
    }

    /**
     * Used for fetching individual car from "car" table based on its ID
     *
     * @param id Integer
     * @return Car
     */
    @Override
    public Car findById(Integer id) {
        return carDAO.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Car with id " + id + " not found"));
    }

    /**
     * Used for fetching individual car from "car" table based on its name
     *
     * @param name String
     * @return Car
     */
    @Override
    public Page<Car> findByName(Pageable pageable, String name) {
        return carDAO.findByNameIgnoreCase(pageable, name);
    }

    /**
     * Used to store new car in "car" table
     *
     * @param car Car
     * @return Car
     */
    @Transactional
    @Override
    public Car create(Integer brandId, Integer partId, Car car) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        car.setBrand(brandService.findById(brandId));
        car.setPart(partService.findById(partId));
        Set<ConstraintViolation<Car>> violations = validator.validate(car);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        return carDAO.save(car);
    }

    /**
     * Used to update existing car under specified id
     *
     * @param carId      Integer
     * @param carRequest Car
     * @return Car
     */
    @Transactional
    @Override
    public Car update(Integer carId, Car carRequest) {
        return carDAO.findById(carId).map(car -> {
            BeanUtils.copyProperties(carRequest, car, CustomBeanUtils.getNullPropertyNames(carRequest));
            return carDAO.save(car);
        }).orElseThrow(() ->
                new ResourceNotFoundException("Car with id " + carId + " not found"));
    }

    /**
     * Used to remove car from "car" table by provided id
     *
     * @param carId Integer
     */
    @Transactional
    @Override
    public void delete(Integer carId) {
        carDAO.deleteById(carId);
    }

    /**
     * Used to remove car from "car" table by provided part Id
     *
     * @param partId Integer
     */
    @Transactional
    @Override
    public void deleteByPart(Integer partId) {
        carDAO.deleteByPart(partId);
    }

    /**
     * Used to remove car from "car" table by provided brand Id
     *
     * @param brandId Integer
     */
    @Transactional
    @Override
    public void deleteByBrand(Integer brandId) {
        carDAO.deleteByBrand(brandId);
    }

    /**
     * Used for fetching all parts from "car" table by car and brand
     *
     * @param pageable Pageable
     * @return Page<CarAndBrand>
     */
    @Override
    public Page<CarAndBrand> getParts(Pageable pageable) {
        return carDAO.getParts(pageable);
    }

    /**
     * Used for fetching all parts from "car" table by car and brand
     *
     * @param car   String
     * @param brand String
     * @return CarAndBrand
     */
    @Override
    public CarAndBrand getPartsByCarAndBrand(String car, String brand) {
        return carDAO.getPartsForCarAndBrand(car, brand).orElseThrow(
                () -> new ResourceNotFoundException("There are no parts for car " + car + " and brand " + brand));
    }
}

package com.auto.demo.service.warehouse;

import com.auto.demo.database.model.warehouse.Part;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public interface PartService {

    /**
     * Interface for fetching all parts from "part" table
     *
     * @param pageable Pageable
     * @return Part
     */
    Page<Part> findAll(Pageable pageable);

    /**
     * Interface for fetching individual part from "part" table based on its ID
     *
     * @param id Integer
     * @return Part
     */
    Part findById(Integer id);

    /**
     * Interface for fetching individual part from "part" table based on its name
     *
     * @param name String
     * @return Part
     */
    Part findByName(String name);

    /**
     * Interface for fetching individual part from "part" table based on its serial number
     *
     * @param serialNumber Integer
     * @return Part
     */
    Part findBySerialNumber(Integer serialNumber);

    /**
     * Interface for fetching individual part from "part" table based on its manufactured date
     *
     * @param date Date
     * @return Part
     */
    Page<Part> findByDate(LocalDate date, Pageable pageable);

    /**
     * Interface to store new part in "part" table
     *
     * @param part Part
     * @return Part
     */
    Part create(Part part);

    /**
     * Interface to update existing part
     *
     * @param partId      Integer
     * @param partRequest Part
     * @return Part
     */
    Part update(Integer partId, Part partRequest);

    /**
     * Interface to remove album from "album" table by provided id
     *
     * @param partId Integer
     */
    void delete(Integer partId);
}

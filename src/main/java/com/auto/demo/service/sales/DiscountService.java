package com.auto.demo.service.sales;

import com.auto.demo.database.model.sale.Discount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;


@Component
public interface DiscountService {

    /**
     * Interface for fetching all discounts from "discount" table
     *
     * @param pageable Pageable
     * @return Page<Discount>
     */
    Page<Discount> findAll(Pageable pageable);

    /**
     * Interface for fetching individual discount from "discount" table based on its ID
     *
     * @param id Integer
     * @return Discount
     */
    Discount findById(Integer id);

    /**
     * Interface for fetching discount from "discount" table based on start date
     *
     * @param start    Date
     * @param pageable Pageable
     * @return Discount
     */
    Page<Discount> findByStart(Pageable pageable, LocalDate start);

    /**
     * Interface for fetching discount from "discount" table based on end date
     *
     * @param end Date
     * @return Discount
     */
    Page<Discount> findByEnd(Pageable pageable, LocalDate end);

    /**
     * Interface for fetching discount from "discount" table based on articleId
     *
     * @param articleId Integer
     * @return Discount
     */
    Discount findByArticleId(Integer articleId);

    /**
     * Interface to store new discount in "discount" table
     *
     * @param discount  Integer
     * @param articleId Discount
     * @return Discount
     */
    Discount create(Integer articleId, Discount discount);

    /**
     * Interface to update existing discount by article Id
     *
     * @param articleId       Integer
     * @param discountRequest Discount
     * @return Discount
     */
    Discount updateByArticleId(Integer articleId, Discount discountRequest);

    /**
     * Interface to update existing discount
     *
     * @param discountId      Integer
     * @param discountRequest Discount
     * @return Discount
     */
    Discount update(Integer discountId, Discount discountRequest);

    /**
     * Interface to remove discount from "discount" table by article id
     *
     * @param articleId Integer
     * @return ResponseEntity<?>
     */
    void deleteByArticle(Integer articleId);

    /**
     * Interface to remove discount from "discount" table by provided id
     *
     * @param discountId Integer
     * @return ResponseEntity<?>
     */
    void delete(Integer discountId);
}

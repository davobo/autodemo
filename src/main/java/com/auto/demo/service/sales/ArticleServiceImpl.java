package com.auto.demo.service.sales;

import com.auto.demo.common.util.CustomBeanUtils;
import com.auto.demo.database.dao.sale.ArticleDAO;
import com.auto.demo.database.dao.sale.DiscountDAO;
import com.auto.demo.database.dao.warehouse.PartDAO;
import com.auto.demo.database.model.sale.Article;
import com.auto.demo.database.model.sale.Offer;
import com.auto.demo.exception.ResourceNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ArticleServiceImpl implements ArticleService {

    /**
     * Object used to perform CRUD operations on "article" table
     */
    private final ArticleDAO articleDAO;

    /**
     * Object used to perform CRUD operations on "part" table
     */
    private final PartDAO partDAO;

    /**
     * Object used to perform CRUD operations on "discount" table
     */
    private final DiscountDAO discountDAO;

    /**
     * Constructor for ArticleController class
     *
     * @param articleDAO  ArticleDAO
     * @param partDAO     PartDAO
     * @param discountDAO DiscountDAO
     */
    public ArticleServiceImpl(ArticleDAO articleDAO, PartDAO partDAO, DiscountDAO discountDAO) {
        this.articleDAO = articleDAO;
        this.partDAO = partDAO;
        this.discountDAO = discountDAO;
    }

    /**
     * Used for fetching all articles from "article" table
     *
     * @param pageable Pageable
     * @return Page<Article>
     */
    @Override
    public Page<Article> findAll(Pageable pageable) {
        return articleDAO.findAll(pageable);
    }

    /**
     * Used for fetching individual article from "article" table by Id
     *
     * @param id Integer
     * @return Article
     */
    @Override
    public Article findById(Integer id) {
        return articleDAO.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Article with id " + id + " not found"));
    }

    /**
     * Used for fetching individual article from "article" table by part serial number
     *
     * @param partSerial Integer
     * @return Article
     */
    @Override
    public Article findByPartSerial(Integer partSerial) {
        return articleDAO.findByPartSerial(partSerial).orElseThrow(
                () -> new ResourceNotFoundException("Article with part serial number " + partSerial + " not found"));
    }

    /**
     * Used for fetching all articles from "article" table as offer
     *
     * @param pageable Pageable
     * @return Page<Article>
     */
    @Override
    public Page<Offer> getOffer(Pageable pageable) {
        List<ArticleDAO.offerInterface> offerObject = articleDAO.getOffer(pageable);
        List<Offer> offerList = offerObject.stream().map(offerInt ->
                new Offer(offerInt.getPartSerial(),
                        Instant.ofEpochMilli(offerInt.getDateManufactured().getTime()).atZone(ZoneId.systemDefault()).toLocalDate(),
                        offerInt.getPrice())).collect(Collectors.toList());
        return new PageImpl<>(offerList);
    }

    /**
     * Used to store new article in "article" table
     *
     * @param partSerial Integer
     * @param article    Article
     * @return Article
     */
    @Transactional
    @Override
    public Article create(Integer partSerial, Article article) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        article.setPartSerial(partDAO.findBySerialNum(partSerial).orElseThrow(() ->
                new ResourceNotFoundException("Part with serial number " + partSerial + " not found")));
        Set<ConstraintViolation<Article>> violations = validator.validate(article);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        return articleDAO.save(article);
    }

    /**
     * Used to update existing article under specified part serial
     *
     * @param partId         Integer
     * @param articleRequest Article
     * @return Article
     */
    @Transactional
    @Override
    public Article updateByPartSerial(Integer partId, Article articleRequest) {
        return articleDAO.findByPartSerial(partId).map(article -> {
            BeanUtils.copyProperties(articleRequest, article, CustomBeanUtils.getNullPropertyNames(articleRequest));
            return articleDAO.save(article);
        }).orElseThrow(() ->
                new ResourceNotFoundException("Article with part " + partId + " not found"));
    }

    /**
     * Used to update existing article under specified id
     *
     * @param articleId      Integer
     * @param articleRequest Article
     * @return Article
     */
    @Transactional
    @Override
    public Article update(Integer articleId, Article articleRequest) {
        return articleDAO.findById(articleId).map(article -> {
            BeanUtils.copyProperties(articleRequest, article, CustomBeanUtils.getNullPropertyNames(articleRequest));
            return articleDAO.save(article);
        }).orElseThrow(() ->
                new ResourceNotFoundException("Article with ID " + articleId + " not found"));
    }

    /**
     * Used to remove article from "article" table by provided id
     *
     * @param articleId Integer
     */
    @Transactional
    @Override
    public void delete(Integer articleId) {
        // First remove any FK restraints
        discountDAO.deleteByArticle(articleId);
        articleDAO.deleteById(articleId);
    }

    /**
     * Used to remove article from "article" table by provided part id
     *
     * @param partId Integer
     */
    @Transactional
    @Override
    public void deleteByPartId(Integer partId) {
        Optional<Article> article = articleDAO.findByPartId(partId);
        if (article.isPresent()) {
            discountDAO.deleteByArticle(article.get().getArticleId());
            articleDAO.deleteById(article.get().getArticleId());
        }
    }
}

package com.auto.demo.service.sales;

import com.auto.demo.common.util.CustomBeanUtils;
import com.auto.demo.database.dao.sale.ArticleDAO;
import com.auto.demo.database.dao.sale.DiscountDAO;
import com.auto.demo.database.model.sale.Discount;
import com.auto.demo.exception.ResourceNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

@Service
public class DiscountServiceImpl implements DiscountService {

    /**
     * Object used to perform CRUD operations on "discount" table
     */
    private final DiscountDAO discountDAO;

    /**
     * Object used to perform CRUD operations on "article" table
     */
    private final ArticleDAO articleDAO;

    /**
     * Constructor for DiscountController class
     *
     * @param discountDAO DiscountDAO
     * @param articleDAO  ArticleDAO
     */
    public DiscountServiceImpl(DiscountDAO discountDAO, ArticleDAO articleDAO) {
        this.discountDAO = discountDAO;
        this.articleDAO = articleDAO;
    }

    /**
     * Used for fetching all discounts from "article" table
     *
     * @param pageable Pageable
     * @return Page<Discount>
     */
    @Override
    public Page<Discount> findAll(Pageable pageable) {
        return discountDAO.findAll(pageable);
    }

    /**
     * Used for fetching individual discount from "discount" table by Id
     *
     * @param id Integer
     * @return Discount
     */
    @Override
    public Discount findById(Integer id) {
        return discountDAO.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Discount with id " + id + " not found"));
    }

    /**
     * Used for fetching discounts from "discount" table based on start date
     *
     * @param start    Date
     * @param pageable Pageable
     * @return Discount
     */
    @Override
    public Page<Discount> findByStart(Pageable pageable, LocalDate start) {
        return discountDAO.findByStart(pageable, start);
    }

    /**
     * Used for fetching discounts from "discount" table based on end date
     *
     * @param end      LocalDate
     * @param pageable Pageable
     * @return Discount
     */
    @Override
    public Page<Discount> findByEnd(Pageable pageable, LocalDate end) {
        return discountDAO.findByEnd(pageable, end);
    }

    /**
     * Used for fetching individual discount from "discount" table by article ID
     *
     * @param articleId Integer
     * @return Article
     */
    @Override
    public Discount findByArticleId(Integer articleId) {
        return discountDAO.findByArticleId(articleId).orElseThrow(
                () -> new ResourceNotFoundException("Discount with article Id: " + articleId + " not found"));
    }

    /**
     * Used to store new discount in "discount" table
     *
     * @param articleId Integer
     * @param discount  Discount
     * @return Discount
     */
    @Transactional
    @Override
    public Discount create(Integer articleId, Discount discount) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        discount.setArticle(articleDAO.findById(articleId).orElseThrow(() ->
                new ResourceNotFoundException("Article with Id " + articleId + " not found")));
        Set<ConstraintViolation<Discount>> violations = validator.validate(discount);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        return discountDAO.save(discount);
    }

    /**
     * Used to update existing discount under specified article Id
     *
     * @param articleId       Integer
     * @param discountRequest Discount
     * @return Discount
     */
    @Transactional
    @Override
    public Discount updateByArticleId(Integer articleId, Discount discountRequest) {
        return discountDAO.findByArticleId(articleId).map(discount -> {
            BeanUtils.copyProperties(discountRequest, discount, CustomBeanUtils.getNullPropertyNames(discountRequest));
            return discountDAO.save(discount);
        }).orElseThrow(() ->
                new ResourceNotFoundException("Discount for article " + articleId + " not found"));
    }

    /**
     * Used to update existing discount under specified id
     *
     * @param discountId      Integer
     * @param discountRequest Discount
     * @return Discount
     */
    @Transactional
    @Override
    public Discount update(Integer discountId, Discount discountRequest) {
        Optional<Discount> dbDiscount = discountDAO.findById(discountId);
        return discountDAO.findById(discountId).map(discount -> {
            BeanUtils.copyProperties(discountRequest, discount, CustomBeanUtils.getNullPropertyNames(discountRequest));
            return discountDAO.save(discount);
        }).orElseThrow(() ->
                new ResourceNotFoundException("Discount with ID: " + discountId + " not found"));
    }

    /**
     * Used to remove discount from "discount" table by provided id
     *
     * @param articleId Integer
     */
    @Transactional
    @Override
    public void deleteByArticle(Integer articleId) {
        discountDAO.deleteByArticle(articleId);
    }

    /**
     * Used to remove discount from "discount" table by provided id
     *
     * @param discountId Integer
     */
    @Transactional
    @Override
    public void delete(Integer discountId) {
        discountDAO.deleteById(discountId);
    }
}

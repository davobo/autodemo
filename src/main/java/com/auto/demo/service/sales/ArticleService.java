package com.auto.demo.service.sales;

import com.auto.demo.database.model.sale.Article;
import com.auto.demo.database.model.sale.Offer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface ArticleService {

    /**
     * Interface for fetching all articles from "article" table
     *
     * @param pageable Pageable
     * @return Page<Article>
     */
    Page<Article> findAll(Pageable pageable);

    /**
     * Interface for fetching individual article from "article" table based on its ID
     *
     * @param id Integer
     * @return Article
     */
    Article findById(Integer id);

    /**
     * Interface for fetching articles from "article" table based on part serial
     *
     * @param partSerial Integer
     * @return Article
     */
    Article findByPartSerial(Integer partSerial);

    /**
     * Interface for fetching all articles from "article" table as offer
     *
     * @param pageable Pageable
     * @return Page<Article>
     */
    Page<Offer> getOffer(Pageable pageable);

    /**
     * Interface to store new article in "article" table
     *
     * @param partSerial Integer
     * @param article    Article
     * @return Article
     */
    Article create(Integer partSerial, Article article);

    /**
     * Interface to update existing article by part serial
     *
     * @param partId         Integer
     * @param articleRequest Article
     * @return Article
     */
    Article updateByPartSerial(Integer partId, Article articleRequest);

    /**
     * Interface to update existing article
     *
     * @param articleId      Integer
     * @param articleRequest Article
     * @return Article
     */
    Article update(Integer articleId, Article articleRequest);

    /**
     * Interface to remove article from "article" table by provided id
     *
     * @param articleId Integer
     */
    void delete(Integer articleId);

    /**
     * Interface to remove article from "article" table by provided part id
     *
     * @param partId Integer
     */
    void deleteByPartId(Integer partId);
}

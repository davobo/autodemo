package com.auto.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.hateoas.config.EnableHypermediaSupport;

@SpringBootApplication
public class AutoDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoDemoApplication.class, args);
    }

}

package com.auto.demo.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Error response that is returned in JSON
 */

@Getter
@Setter
public class ErrorResponse {

    /**
     * Object representing error common name
     */
    private String error;

    /**
     * Object representing error message
     */
    private String message;

    /**
     * Object representing error code
     */
    int code;

    /**
     * Object representing error timestamp
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    LocalDateTime timestamp;

    /**
     * Error response constructor
     *
     * @param error   String
     * @param code    Integer
     * @param message String
     */
    public ErrorResponse(String error, Integer code, String message) {
        this.code = code;
        this.error = error;
        this.message = message;
    }
}

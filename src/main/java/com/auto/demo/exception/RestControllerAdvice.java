package com.auto.demo.exception;

import javassist.NotFoundException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * Central point for handling API errors
 * TODO:rfc7807 format
 */
@ControllerAdvice(basePackages = {"com.auto.demo.controller"})
public class RestControllerAdvice extends ResponseEntityExceptionHandler {

    /**
     * Method used for handling "not found" errors
     *
     * @param ex      ResourceNotFoundException
     * @param request HttpServletRequest
     * @return ResponseEntity<ErrorResponse>
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<ErrorResponse> handleUserNotFoundException
    (ResourceNotFoundException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND.value(), ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    /**
     * Method used for handling "illegal argument" errors
     *
     * @param ex      IllegalArgumentException
     * @param request HttpServletRequest
     * @return ResponseEntity<ErrorResponse>
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(IllegalArgumentException.class)
    public final ResponseEntity<ErrorResponse> handleIllegalArgumentException
    (IllegalArgumentException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND.value(), ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    /**
     * Method used for handling "Method Argument Type Mismatch" errors
     *
     * @param ex      MethodArgumentTypeMismatchException
     * @param request HttpServletRequest
     * @return ResponseEntity<ErrorResponse>
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public final ResponseEntity<ErrorResponse> handleArgumentTypeMismatchException
    (MethodArgumentTypeMismatchException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(),
                HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    /**
     * Method used for handling "Number Format Exception" errors
     *
     * @param ex      NumberFormatException
     * @param request HttpServletRequest
     * @return ResponseEntity<ErrorResponse>
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NumberFormatException.class)
    public final ResponseEntity<ErrorResponse> handleArgumentTypeMismatchException
    (NumberFormatException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(),
                HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    /**
     * Method used for handling "Not Found Exception" errors
     *
     * @param ex      NotFoundException
     * @param request HttpServletRequest
     * @return ResponseEntity<ErrorResponse>
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ErrorResponse> handleNotFoundException
    (NotFoundException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND.value(), ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    /**
     * Method used for handling "Not Found Exception" errors
     *
     * @param ex      NotFoundException
     * @param request HttpServletRequest
     * @return ResponseEntity<ErrorResponse>
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(PropertyReferenceException.class)
    public final ResponseEntity<ErrorResponse> handleNotFoundException
    (PropertyReferenceException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND.value(), ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    /**
     * Method used for handling "Http client exception" errors
     *
     * @param ex      NotFoundException
     * @param request HttpServletRequest
     * @return ResponseEntity<ErrorResponse>
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpClientErrorException.class)
    public final ResponseEntity<ErrorResponse> handleHttpClientException
    (HttpClientErrorException ex, HttpServletRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(),
                HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}

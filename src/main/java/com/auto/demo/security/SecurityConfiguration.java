package com.auto.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api").permitAll()
                .antMatchers(HttpMethod.GET, "/api/sale/**").access("hasAnyRole('SALE', 'KUPAC')")
                .antMatchers(HttpMethod.POST, "/api/sale/**").access("hasRole('SALE')")
                .antMatchers(HttpMethod.PUT, "/api/sale/**").access("hasRole('SALE')")
                .antMatchers(HttpMethod.DELETE, "/api/sale/**").access("hasRole('SALE')")
                .antMatchers("/api/warehouse/**").access("hasRole('WAREHOUSE')")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .failureForwardUrl("/api")
                .and()
                .httpBasic()
                .and()
                .logout()
                .logoutSuccessUrl("/api")
                .deleteCookies("JSESSIONID")
                .and().csrf().disable();
        ;
    }

    @Override
    public void configure(WebSecurity webSecurity) {
        webSecurity.ignoring().antMatchers("/h2-console/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("sales")
                .password(passwordEncoder().encode("prodaja"))
                .roles("SALE")
                .and()
                .withUser("warehouse")
                .password(passwordEncoder().encode("skladiste"))
                .roles("WAREHOUSE")
                .and()
                .withUser("customer")
                .password(passwordEncoder().encode("kupac"))
                .roles("KUPAC");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

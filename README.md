# README #

This is Spring boot + react demo application.

### What is this repository for? ###

 Application is demo version of car part shop api.

* Quick summary
* Version 1.0.0

### How do I get set up? ###

To install this example application, run the following commands:

```bash
git clone https://davobo@bitbucket.org/davobo/autodemo.git
cd autoDemo
```

This will get a copy of the project installed locally. To install all of its dependencies and start each app, follow the instructions below.

To run the server, run:
 
```bash
mvnw spring-boot:run
```

Defalt page URL:
 
```bash
http://localhost:8080
```

Warehouse base URL:

```bash
http://localhost:8080/api/warehouse
```

Sale base URL:

```bash
http://localhost:8080/api/sale
```

OpenAPI documentation:

```bash
http://localhost:8080/api
```

### Application example ###
![Preview](Preview.png)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact